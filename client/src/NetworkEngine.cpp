#include	<iostream>

#include	"NetworkEngine.h"
#include	"PackageRFC.h"

NetworkEngine::NetworkEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL)
	: Engine(texturesM, playersM, missilesL, enemyL, explosionL)
{
}

NetworkEngine::~NetworkEngine()
{
}

void	*routineNetworkEngine(void *arg)
{
	NetworkEngine	*n = reinterpret_cast<NetworkEngine *>(arg);

	n->frame();
	return NULL;
}

void	NetworkEngine::frame()
{
	PackageRFC		p;
	t_package		package;
	std::string		remoteAdress;
	unsigned short	remotePort;
	int				bitsRecv;

	this->sendConnectMsg(this->ipServer, 4242);
	while (42)
	{
		if ((bitsRecv = this->socket.receive(&package, sizeof(package), remoteAdress, remotePort)) == -1)

		p.unPack(package);
	}
}

bool	NetworkEngine::sendConnectMsg(std::string const& address, unsigned int port)
{
	this->p.setType(0x03);

	if (this->socket.send(&(this->p.pack()), sizeof(t_package), address, port) == -1)
		return false;

	return true;
}

bool	NetworkEngine::sendMyPos(std::string const& address, unsigned int port)
{
	//this->p.setType(0x03);
	return true;
}

void	NetworkEngine::setIpServer(std::string const& s)
{
	this->ipServer = s;
}