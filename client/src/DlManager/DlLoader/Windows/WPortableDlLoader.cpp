#ifdef		_WIN32

# include	"PortableDlLoader.h"

PortableDlLoader::PortableDlLoader()
	: lHandle(NULL)
{
}

PortableDlLoader::~PortableDlLoader()
{
	this->unload();
}

bool	PortableDlLoader::load(std::string const& libraryName)
{
	this->lHandle = LoadLibrary(libraryName.c_str());
	if (this->lHandle != NULL)
		return true;
	return false;
}

void	PortableDlLoader::unload()
{
	if (this->lHandle != NULL)
	{
		FreeLibrary(this->lHandle);
		this->lHandle = NULL;
	}
}

void					PortableDlLoader::setHandle(libraryHandle handle)
{
	this->lHandle = handle;
}

libraryHandle const&	PortableDlLoader::getHandle() const
{
	return this->lHandle;
}

template<typename T>
T*		PortableDlLoader::getInstance(std::string const& entrypoint)
{
	T *(*ptr_func)();

	if (this->lHandle)
	{
		ptr_func = reinterpret_cast<T*(*)()>(GetProcAddress(this->lHandle, entrypoint.c_str()));
		if (ptr_func)
			return (ptr_func)();
	}
	return NULL;
}

#endif		// _WIN32