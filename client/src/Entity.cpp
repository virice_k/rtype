#include	"Entity.h"
#include	"GameWindow.h"

#include	<iostream>

Entity::~Entity()
{

}

void	Entity::draw()
{
	this->animSprite.update(GameWindow::getInstance().getDeltaTime());
	GameWindow::getInstance().draw(this->animSprite);
}
	
void	Entity::setPosition(float x, float y)
{
	this->posX = x;
	this->posY = y;
	this->animSprite.setPosition(x, y);
}

void	Entity::setX(float x)
{
	this->posX = x;
	this->animSprite.setPosition(x, this->animSprite.getPosition().y);
}

void	Entity::setY(float y)
{
	this->posY = y;
	this->animSprite.setPosition(this->animSprite.getPosition().x, y);
}

sf::FloatRect	Entity::getLocalBounds() const
{
	return this->animSprite.getLocalBounds();
}

sf::FloatRect	Entity::getGlobalBounds() const
{
	return this->animSprite.getGlobalBounds();
}

float	Entity::getX() const
{
	return this->posX;
}

float	Entity::getY() const
{
	return this->posY;
}

void	Entity::addAnimStep(sf::IntRect const& rect)
{
	this->animation.addStep(rect);
	this->animSprite.setFrame(0);
}

void	Entity::setAnimSpeed(sf::Time time)
{
	this->animSprite.setFrameTime(time);
}

void	Entity::setAnimType(AnimatedSprite::Type t)
{
	this->animSprite.setType(t);
}

Entity::Entity(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y)
	: speed(speed), posX(x), posY(y)
{
	animation.setSpriteSheet(t);
	animation.addStep(r);
	this->animSprite.setAnimation(this->animation);
	this->animSprite.setPosition(x, y);
}

sf::IntRect const& Entity::getTextureRect() const
{
	return this->animation.getStep(this->animSprite.getFrame());
}

void	Entity::moveUp()
{
	this->setPosition(this->posX, this->posY - this->speed * GameWindow::getInstance().getFps());
}

void	Entity::moveDown()
{
	this->setPosition(this->posX, this->posY + this->speed * GameWindow::getInstance().getFps());
}

void	Entity::moveLeft()
{
	this->setPosition(this->posX - this->speed * GameWindow::getInstance().getFps(), this->posY);
}

void	Entity::moveRight()
{
	this->setPosition(this->posX + this->speed * GameWindow::getInstance().getFps(), this->posY);
}