#include	"Starfield.h"
#include	"GameWindow.h"

Starfield::Starfield(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y)
	: Entity(t, r, speed, x, y)
{
	this->s1.setTexture(t);
	this->s2.setTexture(t);

	this->s2.setPosition(this->s1.getPosition().x + this->s1.getLocalBounds().width, 0);
}

Starfield::~Starfield()
{
}

void	Starfield::update()
{
	this->s1.move(this->speed * GameWindow::getInstance().getFps(), 0);
	this->s2.move(this->speed * GameWindow::getInstance().getFps(), 0);
		if (this->s1.getLocalBounds().width + this->s1.getPosition().x < 0)
			this->s1.setPosition(this->s2.getPosition().x + this->s2.getLocalBounds().width, 0);

		if (this->s2.getLocalBounds().width + this->s2.getPosition().x < 0)
			this->s2.setPosition(this->s1.getPosition().x + this->s1.getLocalBounds().width, 0);
}

void	Starfield::draw()
{
	GameWindow::getInstance().draw(this->s1);
	GameWindow::getInstance().draw(this->s2);
}