#include	"LittleEnemy.h"

LittleEnemy::LittleEnemy(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y)
	: Enemy(t, r, speed, x, y)
{
}

LittleEnemy::~LittleEnemy()
{
}

void	LittleEnemy::update()
{
	Entity::moveLeft();
}