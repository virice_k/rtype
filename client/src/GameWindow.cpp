#include	"GameWindow.h"

GameWindow	GameWindow::instance = GameWindow();

GameWindow::GameWindow()
{
	this->window.create(sf::VideoMode(WIN_WIDTH, WIN_HEIGHT), "Rtype", sf::Style::Titlebar | sf::Style::Close);
	this->window.setVerticalSyncEnabled(true);
	this->window.setFramerateLimit(60);
}

GameWindow::~GameWindow()
{
}

GameWindow&	GameWindow::getInstance()
{
	return GameWindow::instance;
}

bool 		GameWindow::isOpen() const
{
	return this->window.isOpen();
}

void 		GameWindow::clear()
{
	this->window.clear();
}

void 		GameWindow::close()
{
	this->window.close();
}

bool 		GameWindow::pollEvent(sf::Event &event)
{
	return this->window.pollEvent(event);
}

void 		GameWindow::display()
{
	this->window.display();
}

void 		GameWindow::draw(sf::Drawable const &drawable, sf::RenderStates const &states)
{
	this->window.draw(drawable, states);
}

void 		GameWindow::draw(sf::Vertex const *vertices, unsigned int vertexCount, sf::PrimitiveType type, sf::RenderStates const &states)
{
	this->window.draw(vertices, vertexCount, type, states);
}

void 		GameWindow::restartClock()
{
	this->deltaTime = this->clock.restart();
	this->fps = 1.f / this->deltaTime.asSeconds();
}

float		GameWindow::getFps() const
{
	return this->fps;
}

sf::Time const&	GameWindow::getDeltaTime() const
{
	return this->deltaTime;
}
