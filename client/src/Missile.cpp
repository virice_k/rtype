#include	"Missile.h"
#include	"Game.h"

Missile::Missile(sf::Texture const& texture, sf::IntRect const& Rect, Player &owner)
	: Entity(texture, Rect, 0.3f), power(1), owner(owner)
{
	sf::IntRect r = owner.getTextureRect();

	this->setPosition(owner.getX() + r.width, owner.getY() + r.height / 2);
}

Missile::~Missile()
{
}

void	Missile::update()
{
	this->moveRight();
}

void	Missile::setPower(int pw)
{
	if (pw <= 100)
		this->power = pw;
}

int		Missile::getPower() const
{
	return this->power;
}

Player	&Missile::getOwner()
{
	return this->owner;
}