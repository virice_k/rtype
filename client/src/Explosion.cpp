#include	"Explosion.h"
#include	"GameWindow.h"

Explosion::Explosion(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y, sf::Time const &duration)
	: Entity(t, r, speed, x, y)
{
	this->duration = duration;
	this->setAnimSpeed(sf::seconds(0.05f));
}

Explosion::~Explosion()
{
}

void	Explosion::update()
{
	this->duration -= GameWindow::getInstance().getDeltaTime();
}

sf::Time const&	Explosion::getDuration() const
{
	return this->duration;
}