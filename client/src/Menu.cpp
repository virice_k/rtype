#include	<cstdlib>
#include	<SFML\Graphics.hpp>
#include	<iostream>
#include	<algorithm>
#include	"Game.h"
#include	"GameWindow.h"
#include	"Menu.h"

Menu::Menu()
{
	pos = 0;
	runmenu = true;
	rungame = true;
	this->text = "Start";
	font.loadFromFile("./resources/font/transformers.ttf");
	start.setFont(font);
	start.setString(text);
	start.setCharacterSize(24);
	start.setPosition(340, 162.5);
	start.setColor(sf::Color::Red);

	text = "Options";
	options.setFont(font);
	options.setString(text);
	options.setCharacterSize(24);
	options.setPosition(340, 212.5);
	options.setColor(sf::Color::White);

	text = "Quitter";
	quitter.setFont(font);
	quitter.setString(text);
	quitter.setCharacterSize(24);
	quitter.setPosition(340, 262.5);
	quitter.setColor(sf::Color::White);
}

Menu::~Menu()
{
}

std::string	const& Menu::GetIpserver() const
{
	return this->ipserver;
}

void		Menu::CheckPos(int dir)
{
	pos = pos + dir;
	if (pos > 2)
		pos = 0;
	else if (pos < 0)
		pos = 2;
	if (pos == 0)
			{
				start.setColor(sf::Color::Red);
				options.setColor(sf::Color::White);
				quitter.setColor(sf::Color::White);
			}
	else if (pos == 1)
			{
				start.setColor(sf::Color::White);
				options.setColor(sf::Color::Red);
				quitter.setColor(sf::Color::White);
			}
	else
			{
				start.setColor(sf::Color::White);
				options.setColor(sf::Color::White);
				quitter.setColor(sf::Color::Red);
			}
}

void		Menu::FillIpServer(char input)
{
	if (ipserver.size() < 16)
	{
		ipserver += input;
		ipcurrent.setFont(font);
		ipcurrent.setString(ipserver);
		ipcurrent.setCharacterSize(22);
		ipcurrent.setPosition(360, 211);
		ipcurrent.setColor(sf::Color::White);
	}
}

void		Menu::BackSpace()
{
	if (ipserver.size() > 0)
		ipserver.resize(ipserver.size()-1);

	ipcurrent.setFont(font);
	ipcurrent.setString(ipserver);
	ipcurrent.setCharacterSize(22);
	ipcurrent.setPosition(360, 211);
	ipcurrent.setColor(sf::Color::White);

}

void		Menu::CheckIp()
{
	bool				Ip;
	sf::Event			event;
	GameWindow			&window = GameWindow::getInstance();

	Ip = true;
	this->text = "Connexion with the server";
	connexion.setFont(font);
	connexion.setString(text);
	connexion.setCharacterSize(24);
	connexion.setPosition(280, 75);
	connexion.setColor(sf::Color::White);

	text = "Ip port = ";
	ipinput.setFont(font);
	ipinput.setString(text);
	ipinput.setCharacterSize(24);
	ipinput.setPosition(200, 207.5);
	ipinput.setColor(sf::Color::White);

	InputBar.setFillColor(sf::Color(0, 0, 0));
	InputBar.setOutlineThickness(1);
	InputBar.setOutlineColor(sf::Color(255, 255, 255));
	InputBar.setPosition(340, 210);
	InputBar.setSize(sf::Vector2f(220, 30));
	while (Ip == true)
	{
		window.clear();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			return ;
		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
				BackSpace();
			else if (event.type == sf::Event::TextEntered)
				{
					if (event.text.unicode < 128)
						FillIpServer(static_cast<char>(event.text.unicode));
					std::cout << ipserver << std::endl;
				}
		}

		window.draw(InputBar);
		window.draw(connexion);
		window.draw(ipinput);
		window.draw(ip);
		window.draw(ipcurrent);
		window.display();
	}
}

void		Menu::CheckMenu()
{
	if (pos == 0)
		runmenu = false;
	else if (pos == 1)
		CheckIp();
	else if (pos == 2)
		{
			runmenu = false;
			rungame = false;
		}
}

bool		Menu::DrawMenu()
{
	GameWindow			&window = GameWindow::getInstance();
	while (runmenu == true)
	{
		while (window.pollEvent(event))
        {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				CheckPos(1);
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				CheckPos(-1);
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
				CheckMenu();
		}
			window.clear();
			window.draw(start);
			window.draw(options);
			window.draw(quitter);
			window.display();
	}
	return rungame;
}