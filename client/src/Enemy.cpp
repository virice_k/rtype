#include	"Enemy.h"

Enemy::Enemy(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y)
	: Entity(t, r, speed, x, y)
{
}

void	Enemy::update()
{
	Entity::moveLeft();
}

Enemy::~Enemy()
{
}