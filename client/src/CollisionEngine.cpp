#include	<iostream>

#include	"CollisionEngine.h"
#include	"Explosion.h"

CollisionEngine::CollisionEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL)
	: Engine(texturesM, playersM, missilesL, enemyL, explosionL)
{
}

CollisionEngine::~CollisionEngine()
{
}

void		CollisionEngine::frame()
{
	std::list<Missile *>::iterator		itMi;
	std::list<Enemy *>::iterator		itEn;
	Explosion							*expl;

	itMi = this->missilesL.begin();
	while (itMi != this->missilesL.end())
	{
		itEn = this->enemyL.begin();
		while(itEn != this->enemyL.end() && itMi != this->missilesL.end())
		{
			if ((*itEn)->getGlobalBounds().intersects((*itMi)->getGlobalBounds()))
			{
				(*itMi)->getOwner().setScore((*itMi)->getOwner().getScore() + 1);
				expl = new Explosion(this->texturesM["explosions"], sf::IntRect(2, 60, 18, 15), 0, (*itMi)->getX(), (*itMi)->getY(), sf::seconds(0.3f));
				expl->addAnimStep(sf::IntRect(30, 57, 27, 20));
				expl->addAnimStep(sf::IntRect(62, 54, 32, 27));
				expl->addAnimStep(sf::IntRect(97, 54, 32, 30));
				expl->addAnimStep(sf::IntRect(132, 54, 32, 30));
				expl->addAnimStep(sf::IntRect(167, 54, 32, 30));
				expl->addAnimStep(sf::IntRect(201, 54, 32, 30));
				this->explosionL.push_back(expl);
				itEn = this->enemyL.erase(itEn);
				if ((*itMi)->getPower() <= 30)
					itMi = this->missilesL.erase(itMi);
				else
					(*itMi)->setPower((*itMi)->getPower() - 15);
			}
			if (itEn != this->enemyL.end())
				++itEn;
		}
		if (itMi != this->missilesL.end())
			++itMi;
	}
}