#include	"EventEngine.h"
#include	"SoundEngine.h"

#include	<iostream>

EventEngine::EventEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL)
	: Engine(texturesM, playersM, missilesL, enemyL, explosionL), window(GameWindow::getInstance())
{
	
	walkingAnimation.setSpriteSheet(texturesM["shoots"]);
	walkingAnimation.addStep(sf::IntRect(260, 80, 5, 16));
    walkingAnimation.addStep(sf::IntRect(1, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(33, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(67	, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(102, 50, 33, 34));
	walkingAnimation.addStep(sf::IntRect(134, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(167, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(200, 50, 33, 34));
    walkingAnimation.addStep(sf::IntRect(232, 50, 33, 34));

    // set up AnimatesSprite
    animatedSprite.setAnimation(walkingAnimation);
	animatedSprite.setFrameTime(sf::seconds(0.07f));
}

EventEngine::~EventEngine()
{
}

int				EventEngine::getPowerMissile() const
{
	return this->powerMissile;
}
void			EventEngine::frame()
{
	static bool	spacePressed = false;
	SoundEngine			&music = SoundEngine::getInstance();

  while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

			if (event.type == sf::Event::KeyReleased)
			{
				if (event.key.code == sf::Keyboard::Space && powerMissile > 30)
				{
					Missile *m = new Missile(this->texturesM["shoots"], sf::IntRect(104, 171, 79, 13), *this->playersM["localPlayer"]);
					m->addAnimStep(sf::IntRect(185, 171, 79, 13));
					m->setAnimSpeed(sf::seconds(0.09f));
					m->setPower(powerMissile);
					this->missilesL.push_back(m);
					music.playSound("tire-charger");
				}
				if (event.key.code == sf::Keyboard::Escape)
				{
					Enemy	*e;

					e = new Enemy(this->texturesM["littleEnemy"], sf::IntRect(5, 6, 21, 24), 0.1f, WIN_WIDTH, WIN_HEIGHT / 2);
					e->addAnimStep(sf::IntRect(38, 6, 21, 24));
					e->addAnimStep(sf::IntRect(71, 6, 21, 24));
					e->addAnimStep(sf::IntRect(104, 6, 21, 24));
					e->addAnimStep(sf::IntRect(137, 6, 21, 24));
					e->addAnimStep(sf::IntRect(170, 6, 21, 24));
					e->addAnimStep(sf::IntRect(203, 6, 21, 24));
					e->addAnimStep(sf::IntRect(236, 6, 21, 24));
					e->setAnimType(AnimatedSprite::PINGPONG);
					this->enemyL.push_back(e);
				}
				if (event.key.code == sf::Keyboard::SemiColon)
				{
					Enemy	*e;

					e = new Enemy(this->texturesM["circleEnemy"], sf::IntRect(7, 40, 22, 22), 0.1f, WIN_WIDTH, WIN_HEIGHT / 2);
					this->enemyL.push_back(e);
				}
				if (event.key.code == sf::Keyboard::BackSpace)
				{
					Enemy	*e;

					e = new Enemy(this->texturesM["blobEnemy"], sf::IntRect(0, 0, 65, 132), 0.1f, WIN_WIDTH, WIN_HEIGHT / 2);
					e->addAnimStep(sf::IntRect(70, 0, 56, 132));
					e->addAnimStep(sf::IntRect(137, 0, 52, 132));
					e->addAnimStep(sf::IntRect(196, 0, 64, 132));
					this->enemyL.push_back(e);
				}
			}
        }


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		this->playersM["localPlayer"]->moveRight();
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		this->playersM["localPlayer"]->moveUp();
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		this->playersM["localPlayer"]->moveLeft();
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		this->playersM["localPlayer"]->moveDown();


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		if (spacePressed == false) // touche press�
		{
			animatedSprite.stop();
			this->missilesL.push_back(new Missile(this->texturesM["shoots"], sf::IntRect(337, 255, 12, 4), *this->playersM["localPlayer"]));
			spacePressed = true;
			powerMissile = 1;
			music.playSound("tire-normal");
		}
		else // touche maintenue
		{
			animatedSprite.play();
			animatedSprite.update(this->window.getDeltaTime());
			animatedSprite.setPosition(this->playersM["localPlayer"]->getX() + 33, this->playersM["localPlayer"]->getY() - 6);
			this->window.draw(this->animatedSprite);
			if (powerMissile < 100)
				++powerMissile;
		}
	}
	else
	{
		spacePressed = false;
		// tir simple
		powerMissile = 1;
	}
}