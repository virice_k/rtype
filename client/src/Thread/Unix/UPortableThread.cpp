#ifndef			_WIN32

# include		<iostream>

# include		"PortableThread.h"

PortableThread::PortableThread()
	: tStartRoutine(NULL), tArg(NULL)
{
}

PortableThread::PortableThread(threadRoutinePointer	startRoutine)
	: tStartRoutine(startRoutine), tArg(NULL)
{
}

PortableThread::PortableThread(threadRoutinePointer startRoutine, threadArgPointer arg)
	: tStartRoutine(startRoutine), tArg(arg)
{
}

PortableThread::~PortableThread()
{
}

void			PortableThread::setRoutine(threadRoutinePointer	startRoutine)
{
	this->tStartRoutine = startRoutine;
}

void			PortableThread::setArg(threadArgPointer arg)
{
	this->tArg = arg;
}

void			PortableThread::start()
{
	if (this->tStartRoutine != NULL)
	{
		if (pthread_create(&this->tHandle, NULL, this->tStartRoutine, this->tArg) != 0)
			std::cerr << "Cannot create thread" << std::endl;
		else
			this->tId = this->tHandle;
	}
	else
		std::cerr << "Missing routine to call this thread" << std::endl;
}

void			PortableThread::wait()
{
	if (pthread_equal(pthread_self(), this->tId) == 0)
		pthread_join(this->tHandle, NULL);
	else
		std::cerr << "WARNING : Thread " << this->tId << " try to wait for itself !" << std::endl;
}

void			PortableThread::exit()
{
	if (this->tHandle)
		pthread_cancel(this->tHandle);
}

#endif			// _WIN32
