#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite(Type t, sf::Time frameTime, bool paused, bool looped) :
    animation(NULL), frameTime(frameTime), currentFrame(0), paused(paused), looped(looped), texture(NULL), t(t)
{
}

void		AnimatedSprite::setAnimation(const Animation& animation)
{
    this->animation = &animation;
    this->texture = this->animation->getSpriteSheet();
    this->currentFrame = 0;
    setFrame(this->currentFrame);
}

void AnimatedSprite::setFrameTime(sf::Time time)
{
    this->frameTime = time;
}

void				AnimatedSprite::setType(Type t)
{
	this->t = t;
}

void AnimatedSprite::play()
{
    this->paused = false;
}

void AnimatedSprite::pause()
{
    this->paused = true;
}

void AnimatedSprite::stop()
{
    this->paused = true;
    this->currentFrame = 0;
    setFrame(this->currentFrame);
}

void AnimatedSprite::setLooped(bool looped)
{
    this->looped = looped;
}

void AnimatedSprite::setColor(sf::Color const& color)
{
    // Update the vertices' color
    this->vertices[0].color = color;
    this->vertices[1].color = color;
    this->vertices[2].color = color;
    this->vertices[3].color = color;
}

Animation const* AnimatedSprite::getAnimation() const
{
    return this->animation;
}

sf::FloatRect		AnimatedSprite::getLocalBounds() const
{
    sf::IntRect rect = this->animation->getStep(this->currentFrame);

    float width = static_cast<float>(std::abs(rect.width));
    float height = static_cast<float>(std::abs(rect.height));

    return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect AnimatedSprite::getGlobalBounds() const
{
    return getTransform().transformRect(getLocalBounds());
}

std::size_t			AnimatedSprite::getFrame() const
{
	return			this->currentFrame;
}

bool AnimatedSprite::isLooped() const
{
    return this->looped;
}

bool AnimatedSprite::isPlaying() const
{
    return !this->paused;
}

sf::Time AnimatedSprite::getFrameTime() const
{
    return this->frameTime;
}

void AnimatedSprite::setFrame(std::size_t newFrame, bool resetTime)
{
    if(this->animation)
    {
        //calculate new vertex positions and texture coordiantes
        sf::IntRect rect = this->animation->getStep(this->currentFrame);

        this->vertices[0].position = sf::Vector2f(0.f, 0.f);
        this->vertices[1].position = sf::Vector2f(0.f, static_cast<float>(rect.height));
        this->vertices[2].position = sf::Vector2f(static_cast<float>(rect.width), static_cast<float>(rect.height));
        this->vertices[3].position = sf::Vector2f(static_cast<float>(rect.width), 0.f);

	static_cast<void>(newFrame);
        float left = static_cast<float>(rect.left) + 0.0001f;
        float right = left + static_cast<float>(rect.width);
        float top = static_cast<float>(rect.top);
        float bottom = top + static_cast<float>(rect.height);

        this->vertices[0].texCoords = sf::Vector2f(left, top);
        this->vertices[1].texCoords = sf::Vector2f(left, bottom);
        this->vertices[2].texCoords = sf::Vector2f(right, bottom);
        this->vertices[3].texCoords = sf::Vector2f(right, top);
    }

    if(resetTime)
        this->currentTime = sf::Time::Zero;
}

void AnimatedSprite::update(sf::Time deltaTime)
{
    // if not paused and we have a valid animation
    if(!this->paused && this->animation)
    {
        // add delta time
        this->currentTime += deltaTime;

        // if current time is bigger then the frame time advance one frame
        if(this->currentTime >= this->frameTime)
        {
            // reset time, but keep the remainder
            this->currentTime = sf::microseconds(this->currentTime.asMicroseconds() % this->frameTime.asMicroseconds());

            // get next Frame index
            if(this->currentFrame + 1 < this->animation->getSize())
                this->currentFrame++;
            else
            {
                // animation has ended
                this->currentFrame = 0; // reset to start

                if(!this->looped)
                {
                    this->paused = true;
                }

            }

            // set the current frame, not reseting the time
            setFrame(this->currentFrame, false);
        }
    }
}

void		AnimatedSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if (this->animation && this->texture)
    {
        states.transform *= getTransform();
        states.texture = this->texture;
        target.draw(this->vertices, 4, sf::Quads, states);
    }
}
