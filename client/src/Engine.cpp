#include	"Engine.h"

Engine::Engine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL)
	: texturesM(texturesM), playersM(playersM), missilesL(missilesL), enemyL(enemyL), explosionL(explosionL)
{
}

Engine::~Engine()
{
}