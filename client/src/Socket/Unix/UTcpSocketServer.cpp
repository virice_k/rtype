#ifndef		_WIN32

# include	<iostream>

# include	"TcpSocketServer.h"

TcpSocketServer::TcpSocketServer()
	: PortableSocket(tcp)
{
	this->create();
}

TcpSocketServer::~TcpSocketServer()
{
}

bool			TcpSocketServer::listen(unsigned short port)
{
	SOCKADDR_IN	service;

	if (this->sock == INVALID_SOCKET)
		return false;

	service.sin_family = AF_INET;
	service.sin_addr.s_addr = INADDR_ANY;

	if (port == 0)
	  return false;
	service.sin_port = htons(port);

	if (::bind(this->sock, (SOCKADDR *)(&service), sizeof(service)) == SOCKET_ERROR)
		return false;

	if (::listen(this->sock, 5) == SOCKET_ERROR)
		return false;
	
	return true;
}

bool			TcpSocketServer::accept(TcpSocketClient& sockClient)
{
  SocketHandle		ret;

  if (this->sock == INVALID_SOCKET)
		return false;

  ret = ::accept(this->sock, NULL, NULL);
  if (ret == INVALID_SOCKET)
    return false;
 
  sockClient.create(ret);
  return true;
}

#endif		// !_WIN32
