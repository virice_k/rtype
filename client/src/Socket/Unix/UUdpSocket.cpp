#ifndef		_WIN32

# include	<iostream>

# include	"UdpSocket.h"

UdpSocket::UdpSocket()
	: PortableSocket(udp)
{
	this->create();
}

UdpSocket::~UdpSocket()
{
}

bool		UdpSocket::bind(unsigned short port)
{
  SOCKADDR_IN	sin;

  if (this->sock == INVALID_SOCKET)
		return false;

  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(port);

  if (sin.sin_port == 0)
    return false;

  if(::bind(this->sock, (SOCKADDR *)(&sin), sizeof(sin)) == SOCKET_ERROR)
    return false;

  return true;
}

int			UdpSocket::send(void const *data, size_t size,
					std::string const& remoteAddress,
					unsigned short remotePort)
{
  int			sendBytes;
  SOCKADDR_IN		RecvAddr;

  if (this->sock == INVALID_SOCKET)
		return false;

  RecvAddr.sin_addr.s_addr = inet_addr(remoteAddress.c_str());
  if (RecvAddr.sin_addr.s_addr == INADDR_NONE)
    return -1;

  RecvAddr.sin_port = htons(remotePort);
  if (RecvAddr.sin_port == 0)
    return -1;

  sendBytes = sendto(this->sock, data, size, 0,
		     (SOCKADDR *)(&RecvAddr), sizeof(RecvAddr));

  return sendBytes;
}

int			UdpSocket::receive(void *data, size_t size,
					   std::string &remoteAddress,
					   unsigned short &remotePort)
{
  int			recvBytes;
  SOCKADDR_IN		SendAddr;
  socklen_t	       	SendAddrSize = sizeof(SendAddr);

  if (this->sock == INVALID_SOCKET)
		return false;

  recvBytes = recvfrom(this->sock, data, size, 0,
		       (SOCKADDR *)(&SendAddr), &SendAddrSize);

  if (recvBytes == -1)
    return -1;

  remoteAddress = inet_ntoa(SendAddr.sin_addr);
  remotePort = ntohs(SendAddr.sin_port);

  return recvBytes;
}

#endif
