#ifdef		_WIN32

# include	<iostream>

# include	"UdpSocket.h"

UdpSocket::UdpSocket()
	: PortableSocket(udp)
{
	this->create();
}

UdpSocket::~UdpSocket()
{
}

bool			UdpSocket::bind(unsigned short port)
{
	SOCKADDR_IN	sin;

	if (this->sock == INVALID_SOCKET)
		return false;

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	if (WSAHtons(this->sock, port, &sin.sin_port) == SOCKET_ERROR)
		return false;

	if (sin.sin_port == 0)
		return false;

	if(::bind(this->sock, (SOCKADDR *) &sin, sizeof(sin)) == SOCKET_ERROR)
		return false;

	return true;
}

int				UdpSocket::send(void const *data, size_t size, std::string const& remoteAddress, unsigned short remotePort)
{
	WSABUF		buffer;
	DWORD		sendBytes;
	DWORD		flag = 0;
	SOCKADDR_IN	RecvAddr;

	if (this->sock == INVALID_SOCKET)
		return false;

	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_addr.s_addr = inet_addr(remoteAddress.c_str());
	if (RecvAddr.sin_addr.s_addr == INADDR_NONE)
		return -1;

	if (WSAHtons(this->sock, remotePort, &RecvAddr.sin_port) == SOCKET_ERROR)
		return -1;
	if (RecvAddr.sin_port == 0)
		return -1;

	buffer.buf = const_cast<CHAR *>(static_cast<CHAR const *>(data));
	buffer.len = size;

	if (WSASendTo(this->sock, &buffer, 1, &sendBytes, flag, (SOCKADDR *)(&RecvAddr), sizeof(RecvAddr), NULL, NULL) == SOCKET_ERROR)
	{
		std::cerr << WSAGetLastError() << std::endl;
		return -1;
	}
	return sendBytes;
}

int				UdpSocket::receive(void *data, size_t size, std::string &remoteAddress, unsigned short &remotePort)
{
	WSABUF		buffer;
	DWORD		recvBytes = 0;
	DWORD		flag = 0;
	SOCKADDR_IN	SendAddr;
	int			SendAddrSize = sizeof(SendAddr);

	if (this->sock == INVALID_SOCKET)
		return -1;

	buffer.buf = static_cast<CHAR *>(data);
	buffer.len = size;

	if (WSARecvFrom(this->sock, &buffer, 1, &recvBytes, &flag, (SOCKADDR *)(&SendAddr), &SendAddrSize, NULL, NULL) == SOCKET_ERROR)
		return -1;
	remoteAddress = inet_ntoa(SendAddr.sin_addr);
	memcpy(data, buffer.buf, buffer.len);
	if (WSANtohs(this->sock, SendAddr.sin_port, &remotePort) == SOCKET_ERROR)
		return -1;

	return buffer.len;
}

#endif
