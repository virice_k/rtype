#ifdef		_WIN32

# include	<iostream>

# include	"TcpSocketClient.h"

TcpSocketClient::TcpSocketClient()
	: PortableSocket(tcp)
{
	this->create();
}

TcpSocketClient::~TcpSocketClient()
{
}

bool		TcpSocketClient::connect(std::string const& host, unsigned short port)
{
	SOCKADDR_IN		sin;

	if (this->sock == INVALID_SOCKET)
		return false;

	sin.sin_addr.s_addr = inet_addr(host.c_str());
	sin.sin_family = AF_INET;
	
	if (WSAHtons(this->sock, port, &sin.sin_port) == SOCKET_ERROR)
		return false;

	if (WSAConnect(this->sock, (SOCKADDR *)(&sin), sizeof(sin), NULL, NULL, NULL, NULL) == SOCKET_ERROR)
		return false;
	
	return true;
}

int				TcpSocketClient::send(void const *data, size_t size)
{
	WSABUF		wsaBuffer;
	DWORD		sendBytes;

	if (this->sock == INVALID_SOCKET)
		return -1;

	wsaBuffer.buf = const_cast<CHAR *>(static_cast<CHAR const*>(data));
	wsaBuffer.len = size;
	
	if (WSASend(this->sock, &wsaBuffer, 1, &sendBytes, 0, NULL, NULL) == SOCKET_ERROR)
	{
		std::cerr << "WSASend failed: " << WSAGetLastError() << std::endl;
		return -1;
	}
	return sendBytes;
}

int				TcpSocketClient::receive(void *data, size_t size)
{
	WSABUF		wsaBuffer;
	DWORD		recvBytes;

	if (this->sock == INVALID_SOCKET)
		return -1;

	wsaBuffer.buf = static_cast<CHAR *>(data);
	wsaBuffer.len = size;
	if (WSARecv(this->sock, &wsaBuffer, 1, &recvBytes, 0, NULL, NULL) == SOCKET_ERROR)
	{
		std::cerr << "WSARecv failed: " << WSAGetLastError() << std::endl;
		return -1;
	}
	return recvBytes;
}

#endif // _WIN32