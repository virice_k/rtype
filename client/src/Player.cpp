#include	"Player.h"
#include	"GameWindow.h"

Player::Player(sf::Texture const& texture, sf::IntRect const& Rect, float x, float y, std::string const& name)
	: Entity(texture, Rect, 0.1f, x, y), name(name), score(0), lifes(3)
{
}

Player::~Player()
{
}

int		Player::getScore() const
{
	return this->score;
}

void		Player::setScore(int s)
{
	this->score = s;
}

void	Player::update()
{
	this->animation.replaceStep(sf::IntRect(67, 3, 32, 12));
	this->animSprite.setFrame(0);
}

void	Player::moveUp()
{
	this->animation.replaceStep(sf::IntRect(133, 3, 32, 12));
	this->animSprite.setFrame(0);
	Entity::moveUp();
	if (this->posY < 0)
		this->setY(0);
}

void	Player::moveDown()
{
	this->animation.replaceStep(sf::IntRect(1, 3, 32, 12));
	this->animSprite.setFrame(0);
	Entity::moveDown();
	if (this->posY + this->animSprite.getLocalBounds().height > WIN_HEIGHT)
		this->setY(WIN_HEIGHT - this->animSprite.getLocalBounds().height);
}

void	Player::moveLeft()
{
	this->animation.replaceStep(sf::IntRect(67, 3, 32, 12));
	this->animSprite.setFrame(0);
	Entity::moveLeft();
	if (this->posX < 0)
		this->setX(0);
}

void	Player::moveRight()
{
	this->animation.replaceStep(sf::IntRect(67, 3, 32, 12));
	this->animSprite.setFrame(0);
	Entity::moveRight();
	if (this->posX + this->animSprite.getLocalBounds().width > WIN_WIDTH)
		this->setX(WIN_WIDTH - this->animSprite.getLocalBounds().width);
}