#include	<cstdlib>
#include	<iostream>

#include	"Game.h"
#include	"RException.h"
#include	"Menu.h"

int			main()
{
Menu		menu;
  Game				game;

  if (menu.DrawMenu())
  {
	  game.setIpServer(menu.GetIpserver());
  try
    {
      game.run();
    }
  catch (RException const& e)
    {
      std::cerr << e.what() << "\n";
    }
  }
  return EXIT_SUCCESS;
}
