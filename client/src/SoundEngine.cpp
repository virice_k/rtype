#include "SoundEngine.h"

SoundEngine SoundEngine::SoundInstance = SoundEngine();

SoundEngine::SoundEngine()
{

}

SoundEngine::~SoundEngine()
{

}

SoundEngine& SoundEngine::getInstance()
{
	return SoundEngine::SoundInstance;
}

bool SoundEngine::addMusic(std::string const& ident, std::string const& path)
{
	sf::Music	*ret;

	ret = new sf::Music;
	if (!ret->openFromFile(path))
		return false;
	this->musics[ident] = ret;
	this->musics[ident]->setLoop(true);
		return true;	
}

bool SoundEngine::addSound(std::string const& ident, std::string const& path)
{
	sf::SoundBuffer *ret;
	sf::Sound		*ret2;

	ret = new sf::SoundBuffer;
	ret2 = new sf::Sound;

	if (!ret->loadFromFile(path))
		return false;
	this->soundBuffers[ident] = ret;
	
	ret2->setBuffer(*this->soundBuffers[ident]);
	this->sounds[ident] = ret2;	
	return true;
}


bool SoundEngine::playMusic(std::string const& ident)
{
	if (this->musics.find(ident) != this->musics.end())
	{
		this->musics[ident]->play();
		return true;
	}
	return false;
}

bool SoundEngine::playSound(std::string const& ident)
{
	if (this->sounds.find(ident) != this->sounds.end())
	{
		this->sounds[ident]->play();
		return true;
	}
	return false;
}
bool SoundEngine::pauseMusic(std::string const& ident)
{
	if (this->musics.find(ident) != this->musics.end())
	{
		this->musics[ident]->pause();
		return true;
	}
	return false;
}

bool SoundEngine::stopMusic(std::string const& ident)
{
	if (this->musics.find(ident) != this->musics.end())
	{
		this->musics[ident]->stop();
		return true;
	}
	return false;
}
