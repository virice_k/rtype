#ifndef		_WIN32

# include	"PortableMutex.h"

PortableMutex::PortableMutex()
	: isInit(false)
{
}

PortableMutex::~PortableMutex()
{
	if (this->isInit)
	{
		pthread_mutex_destroy(&this->mHandle);
		this->isInit = false;
	}
}

void		PortableMutex::init()
{
	if (!this->isInit)
	{
		pthread_mutex_init(&this->mHandle, NULL);
		this->isInit = true;
	}
}

void		PortableMutex::destroy()
{
	if (this->isInit)
	{
		pthread_mutex_destroy(&this->mHandle);
		this->isInit = false;
	}
}

void		PortableMutex::lock()
{
	pthread_mutex_lock(&this->mHandle);
}

bool		PortableMutex::trylock()
{
	if (pthread_mutex_trylock(&this->mHandle) == 0)
		return true;
	return false;
}

void		PortableMutex::unlock()
{
	pthread_mutex_unlock(&this->mHandle);
}

#endif // _WIN32