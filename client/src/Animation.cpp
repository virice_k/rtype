#include	"Animation.h"

Animation::Animation()
	: texture(NULL)
{
}

void	Animation::addStep(sf::IntRect const& rect)
{
	this->steps.push_back(rect);
}

bool	Animation::replaceStep(sf::IntRect const& rect, std::size_t index)
{
	if (index < this->steps.size())
	{
		this->steps[index] = rect;
		return true;
	}
	return false;
}

void	Animation::setSpriteSheet(sf::Texture const& texture)
{
    this->texture = &texture;
}

sf::Texture const*	Animation::getSpriteSheet() const
{
    return this->texture;
}

size_t		Animation::getSize() const
{
	return this->steps.size();
}

sf::IntRect const&	Animation::getStep(size_t n) const
{
	return this->steps[n];
}