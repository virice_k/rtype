#include	<iostream>
#include	<iomanip>
#include	<sstream>
#include	<SFML/Window.hpp>

#include	"Game.h"
#include	"GameWindow.h"
#include	"Player.h"
#include	"SoundEngine.h"
#include	"PortableThread.h"

Game::Game()
	: running(true)
{
	this->eventEngine = new EventEngine(this->texturesM, this->playersM, this->missilesL, this->enemyL, this->explosionL);
	this->networkEngine = new NetworkEngine(this->texturesM, this->playersM, this->missilesL, this->enemyL, this->explosionL);

	this->networkEngine->setIpServer(this->ipServer);

	SoundEngine			&music = SoundEngine::getInstance();

	this->collisionEngine = new CollisionEngine(this->texturesM, this->playersM, this->missilesL, this->enemyL, this->explosionL);

	this->texturesM["ships"].loadFromFile("./resources/sprites/r-typesheet42.gif");
	this->texturesM["shoots"].loadFromFile("./resources/sprites/r-typesheet1.gif");
	this->texturesM["starfield"].loadFromFile("./resources/sprites/starfield.jpg");
	this->texturesM["littleEnemy"].loadFromFile("./resources/sprites/r-typesheet5.gif");
	this->texturesM["blobEnemy"].loadFromFile("./resources/sprites/r-typesheet17.gif");
	this->texturesM["circleEnemy"].loadFromFile("./resources/sprites/r-typesheet8.gif");
	this->texturesM["explosions"].loadFromFile("./resources/sprites/r-typesheet43.gif");

	this->FontsM["score"].loadFromFile("./resources/font/transformers.ttf");

	this->starfield = new Starfield(this->texturesM["starfield"], sf::IntRect(0, 0, 0, 0));

	this->playersM.insert(std::pair<std::string, Player*>("localPlayer", new Player(this->texturesM["ships"], sf::IntRect(67, 3, 32, 12))));

	this->Score.setFont(this->FontsM["score"]);
	this->Beam.setFont(this->FontsM["score"]);
	this->ProgressBar.setSize(sf::Vector2f(100, 10));
	music.addMusic("ambiance", "./resources/musics/test.wav");
	music.addSound("tire-charger", "./resources/musics/tire-charger.wav");
	music.addSound("tire-normal", "./resources/musics/tire-normal.wav");
}

Game::~Game()
{
	Missile		*m;
	Enemy		*e;
	Explosion	*expl;
	delete this->eventEngine;
	delete this->networkEngine;
	delete this->starfield;
	delete this->collisionEngine;

	for (std::map<std::string, Player *>::iterator it = this->playersM.begin(); it != this->playersM.end(); ++it)
	{
		delete it->second;
		it->second = NULL;
	}
	for (std::list<Missile *>::iterator it = this->missilesL.begin(); it != this->missilesL.end(); ++it)
	{
		m = *it;
		delete m;
		m = NULL;
	}
	for (std::list<Enemy *>::iterator it = this->enemyL.begin(); it != this->enemyL.end(); ++it)
	{
		e = *it;
		delete e;
		e = NULL;
	}

	for (std::list<Explosion *>::iterator it =this->explosionL.begin(); it != this->explosionL.end(); ++it)
	{
		expl = *it;
		delete expl;
		expl = NULL;
	}
}

void					Game::DrawScore()
{
	std::ostringstream oss;
	oss << "SCORE : " << std::setfill('0') << std::setw(8) << this->playersM["localPlayer"]->getScore();
	Score.setString(oss.str());
	Score.setCharacterSize(24);
	Score.setPosition(200, 485);
	Score.setColor(sf::Color::White);
}

void					Game::DrawProgressBar(float const &x)
{
	std::string test = "BEAM :";
	this->Beam.setString(test);
	this->Beam.setCharacterSize(24);
	this->Beam.setPosition(200, 465);
	this->Beam.setColor(sf::Color::White);

	this->ProgressBarI.setFillColor(sf::Color(0, 0, 139));
	this->ProgressBarI.setPosition(295, 475);
	this->ProgressBarI.setSize(sf::Vector2f(x, 10));
	
	this->ProgressBar.setFillColor(sf::Color(0, 0, 0));
	this->ProgressBar.setOutlineThickness(1);
	this->ProgressBar.setOutlineColor(sf::Color(255, 255, 255));
	this->ProgressBar.setPosition(295, 475);
}
void					Game::run()
{
	GameWindow			&window = GameWindow::getInstance();
	SoundEngine			&music = SoundEngine::getInstance();
	PortableThread		connectionThread(&routineNetworkEngine, this->networkEngine);

	music.playMusic("ambiance");
	connectionThread.start();
	while (this->running && window.isOpen())
	{
	//	std::cout << window.getFps() << std::endl;
		window.clear();
		this->starfield->draw();
		this->starfield->update();
		this->eventEngine->frame();
		this->collisionEngine->frame();

		window.restartClock();
		
		for (std::map<std::string, Player *>::iterator it = this->playersM.begin(); it != this->playersM.end(); ++it)
		{
			it->second->draw();
			it->second->update();
		}

		for (std::list<Missile *>::iterator it = this->missilesL.begin(); it != this->missilesL.end(); ++it)
		{
			(*it)->draw();
			(*it)->update();
		}

		for (std::list<Enemy *>::iterator it = this->enemyL.begin(); it != this->enemyL.end(); ++it)
		{
			(*it)->draw();
			(*it)->update();
		}
		std::list<Explosion *>::iterator it =this->explosionL.begin();
		while (it != this->explosionL.end())
		{
			(*it)->draw();
			(*it)->update();
			if ((*it)->getDuration() < sf::milliseconds(0))
			{
				delete (*it);
				(*it) = NULL;
				it = this->explosionL.erase(it);
			}
			if (it != this->explosionL.end())
				++it;
		}

		DrawScore();
		DrawProgressBar(static_cast<float>(this->eventEngine->getPowerMissile()));
		window.draw(this->Score);
		window.draw(this->Beam);
		window.draw(this->ProgressBar);
		window.draw(this->ProgressBarI);
		window.display();
	}
	connectionThread.exit();
}

void		Game::stopGame()
{
	this->running = false;
}

void	Game::setIpServer(std::string const &s)
{
	this->ipServer = s;
}

std::string	const&	Game::getIpServer() const
{
	return this->ipServer;
}
