#ifndef		ANIMATEDSPRITE_H
# define	ANIMATEDSPRITE_H

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Vector2.hpp>

#include "Animation.h"

class			AnimatedSprite : public sf::Drawable, public sf::Transformable
{
public:
	enum Type
	{
		LOOP,
		PINGPONG
	};

    AnimatedSprite(Type t = LOOP, sf::Time frameTime = sf::seconds(0.2f), bool paused = false, bool looped = true);

    void				update(sf::Time deltaTime);
    void				setAnimation(Animation const& animation);
    void				setFrameTime(sf::Time time);
    void				play();
    void				pause();
    void				stop();
    void				setLooped(bool looped);
    void				setColor(sf::Color const& color);
	void				setType(Type t);
    Animation const*	getAnimation() const;
    sf::FloatRect		getLocalBounds() const;
    sf::FloatRect		getGlobalBounds() const;
    bool				isLooped() const;
    bool				isPlaying() const;
    sf::Time			getFrameTime() const;
	std::size_t			getFrame() const;
    void				setFrame(std::size_t newFrame, bool resetTime = true);

private:
    Animation const*	animation;
    sf::Time			frameTime;
    sf::Time			currentTime;
    std::size_t			currentFrame;
    bool				paused;
    bool				looped;
    sf::Texture const*	texture;
    sf::Vertex			vertices[4];
	Type				t;

    virtual void		draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // ANIMATEDSPRITE_H