#ifndef		TCPSOCKETCLIENT_H
# define	TCPSOCKETCLIENT_H

# include	"PortableSocket.h"

class		TcpSocketClient : public PortableSocket
{
public:
	TcpSocketClient();
	virtual ~TcpSocketClient();

	bool			connect(std::string const& host, unsigned short port);
	int				send(void const *data, size_t size);
	int				receive(void *data, size_t size);

protected:
	friend class TcpSocketServer;
};
#endif // !TCPSOCKETCLIENT_H
