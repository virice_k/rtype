#ifndef		UPORTABLESOCKET_H
# define	UPORTABLESOCKET_H

# ifndef		_WIN32

#  include		<unistd.h>
#  include		<sys/types.h>
#  include		<sys/socket.h>
#  include		<arpa/inet.h>

#  define	closesocket(s) ::close(s)
#  define	INVALID_SOCKET	-1
#  define	SOCKET_ERROR	-1

typedef		int			SocketHandle;
typedef		struct sockaddr_in	SOCKADDR_IN;
typedef		struct sockaddr		SOCKADDR;
typedef		struct in_addr		IN_ADDR;


#endif	// !_WIN32

#endif // !UPORTABLESOCKET_H
