#ifndef			PACKAGERFC_H_
# define		PACKAGERFC_H_

# include		<ctime>
# include		<string>

typedef		struct s_package
{
	int			id;
	time_t			time;
	char			type;
	char			size;
	char			function;
	char			value[255];
}					t_package;

class PackageRFC
{
public:
	PackageRFC(int = 0, char = 0, char = 0, char = 0, std::string = "");
	PackageRFC(t_package);
	~PackageRFC();

private:
	int			_id;
	time_t		_time;
	char		_type;
	char		_size;
	char		_function;
	std::string	_value;

public:
	int			getId() const;
	std::time_t	getTime() const;
	char		getType() const;
	char		getSize() const;
	char		getFctId() const;
	std::string	getValue() const;

	void		setTime();
	void		setType(char);
	void		setSize(char);
	void		addValue(std::string);
	void		delValue();
	void		replaceValue(std::string);

	t_package	pack() const;
	void		unPack(t_package const&);
};

#endif // !PACKAGERFC_H
