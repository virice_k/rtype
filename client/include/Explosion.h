#ifndef		EXPLOSION_H
# define	EXPLOSION_H

# include	"Entity.h"

class Explosion : public Entity
{
public:
	Explosion(sf::Texture const& t, sf::IntRect const& r, float speed, float x, float y, sf::Time const &duration);
	~Explosion();

	virtual void	update();
	sf::Time const&	getDuration() const;
private:
	sf::Time	duration;
};

#endif // !EXPLOSION_H
