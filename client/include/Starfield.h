#ifndef		STARFIELD_H
# define	STARFIELD_H

# include	"Entity.h"

class		Starfield : public Entity
{
public:
	Starfield(sf::Texture const& t, sf::IntRect const& r, float speed = -0.05f, float x = 0, float y = 0);
	~Starfield();

	virtual void	update();
	virtual void	draw();
private:
	sf::Sprite	s1;
	sf::Sprite	s2;
};

#endif // !STARFIELD_H
