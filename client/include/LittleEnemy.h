#ifndef		LITTLEENEMY_H
# define	LITTLEENEMY_H

# include	"Enemy.h"
# include	"GameWindow.h"

class LittleEnemy : public Enemy
{
public:
	LittleEnemy(sf::Texture const& t, sf::IntRect const& r, float speed, float x = WIN_WIDTH, float y = WIN_HEIGHT / 2);
	~LittleEnemy();

	virtual void	update();

private:
};

#endif // !LITTLEENEMY_H
