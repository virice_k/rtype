#ifndef		REXCEPTION_H
# define	REXCEPTION_H

# include	<iostream>
# include	<sstream>
# include	<exception>
# include	<stdexcept>
# include	<string>

class RException : public std::runtime_error
{
 public:
 
 RException(char const *Msg)
   : runtime_error(Msg)
  {
    this->msg = "error on socket";
  }
 
  virtual ~RException() throw()
    {
 
    }
 
  virtual const char * what() const throw()
  {
    return this->msg.c_str();
  }
 
 private:
  std::string msg;
};

#endif // !REXCEPTION_H
