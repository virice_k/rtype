#ifndef									GRAPHICSENGINE_H
# define								GRAPHICSENGINE_H

# include								<map>
# include								<list>
# include								<SFML/Graphics/RenderWindow.hpp>
# include								<SFML/Graphics/Font.hpp>

# include								"Game.h"
# include								"Engine.h"
# include								"Entity.h"
# include								"EventEngine.h"

class									GraphicsEngine : public Engine
{
public:
	GraphicsEngine(Game *p);
	~GraphicsEngine();

	std::map<std::string, sf::Texture>	textures;
	std::map<std::string, sf::Font>		fonts;

	void					initEventEngine();
	virtual void			frame();
private:
	sf::RenderWindow		window;
	EventEngine				*eventE;
	sf::Sprite				starfield1;
	sf::Sprite				starfield2;

	void					drawFps();
	void					is_in_window(std::list<Entity *>::iterator &it);
};

#endif // !GRAPHICSENGINE_H
