#ifndef		SOUNDENGINE_H
# define	SOUNDENGINE_H

#include	<iostream>
#include	<map>

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

class		SoundEngine
{
public:
	static SoundEngine& getInstance();

	bool addMusic(std::string const& ident, std::string const& path);
	bool addSound(std::string const& ident, std::string const& path);
	//bool delMusic(std::string const& ident);
	//bool delSound(std::string const& ident);
	bool playMusic(std::string const& ident);
	bool playSound(std::string const& ident);
	bool pauseMusic(std::string const& ident);
	bool stopMusic(std::string const& ident);
private:
	SoundEngine();
	~SoundEngine();

	SoundEngine& operator= (const SoundEngine&);
    SoundEngine (const SoundEngine&);

	std::map <std::string, sf::Music *> musics;
	std::map <std::string, sf::SoundBuffer *> soundBuffers;
	std::map <std::string, sf::Sound *> sounds;

	static SoundEngine SoundInstance;
};
#endif // !SOUNDENGINE_H
