#ifndef			GAMEWINDOW_H
# define		GAMEWINDOW_H

# define		WIN_WIDTH	750
# define		WIN_HEIGHT	525

#include		<SFML/Graphics/RenderWindow.hpp>

class			GameWindow
{
public:
	static GameWindow&	getInstance();

	bool 		isOpen() const;
	void 		clear();
	void 		close();
	bool 		pollEvent(sf::Event &event);
	void 		display();
	void 		draw(sf::Drawable const &drawable, sf::RenderStates const &states = sf::RenderStates::Default);
	void 		draw(sf::Vertex const *vertices, unsigned int vertexCount, sf::PrimitiveType type, sf::RenderStates const &states = sf::RenderStates::Default);

	void 		restartClock();
	float		getFps() const;
	sf::Time const&	getDeltaTime() const;

private:
	GameWindow();
	~GameWindow();

	GameWindow(GameWindow const &);
	GameWindow&		operator=(GameWindow const &);

	sf::RenderWindow	window;

	sf::Clock			clock;
	sf::Time			deltaTime;
	float				fps;

	static GameWindow	instance;
};

#endif // !GAMEWINDOW_H
