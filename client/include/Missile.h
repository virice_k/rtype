#ifndef		MISSILE_H
# define	MISSILE_H

# include <string>

# include	"Entity.h"
# include	"Player.h"

class Missile : public Entity
{
public:
	Missile(sf::Texture const&, sf::IntRect const&, Player &);
	~Missile();

	void	update();
	void	setPower(int pw);
	int		getPower() const;
	Player	&getOwner();
private:
	int		power;
	Player	&owner;
};

#endif // !MISSILE_H
