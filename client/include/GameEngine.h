#ifndef									GAMEENGINE_H
# define								GAMEENGINE_H

# include								<map>
# include								<SFML/Graphics/RenderWindow.hpp>
# include								<SFML/Graphics/Font.hpp>

# include								"Game.h"
# include								"Engine.h"

class									GameEngine : public Engine
{
public:
	GameEngine(Game *p);
	~GameEngine();

	GameData							gameData;

	void					addMissile(Entity &owner);
	void					addPlayer();
	virtual void			frame();
};

#endif // !GRAPHICSENGINE_H
