#ifndef		GAME_H
# define	GAME_H

# include	<string>
# include	<list>
# include	<map>
# include	<SFML/Graphics.hpp>

# include	"Player.h"
# include	"EventEngine.h"
# include	"NetworkEngine.h"
# include	"Missile.h"
# include	"Starfield.h"
# include	"Enemy.h"
# include	"Explosion.h"
# include	"CollisionEngine.h"
# include	"PortableMutex.h"

class		Game
{
public:
	Game();
	~Game();

	void	run();
	void	stopGame();
	void	DrawScore();
	void	DrawProgressBar(float const &x);

	void	setIpServer(std::string const &s);
	std::string	const&	getIpServer() const;

private:
	bool								running;

	std::map<std::string, sf::Texture>	texturesM;
	std::map<std::string, Player*>		playersM;
	std::map<std::string, sf::Font>		FontsM;

	std::list<Missile *>				missilesL;
	std::list<Enemy *>					enemyL;
	std::list<Explosion *>				explosionL;

	PortableMutex						playersMutex;

	Starfield							*starfield;

	std::string							ipServer;

	sf::Text							Score;
	sf::Text							Beam;
	sf::RectangleShape					ProgressBar;
	sf::RectangleShape					ProgressBarI;
	EventEngine							*eventEngine;
	NetworkEngine						*networkEngine;
	CollisionEngine						*collisionEngine;
};

#endif // !GAME_H
