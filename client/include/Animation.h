#ifndef		ANIMATION_H
# define	ANIMATION_H

# include	<SFML/Graphics.hpp>
# include	<vector>

class		Animation
{
public:
    Animation();

    void				addStep(sf::IntRect const& rect);
	bool				replaceStep(sf::IntRect const& rect, std::size_t index = 0);

    void				setSpriteSheet(const sf::Texture& texture);
    sf::Texture const*	getSpriteSheet() const;
    std::size_t			getSize() const;
    sf::IntRect const&	getStep(std::size_t n) const;

private:
    std::vector<sf::IntRect>	steps;
    sf::Texture const*			texture;
};

#endif // !ANIMATION_H
