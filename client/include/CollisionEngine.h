#ifndef		COLLISIONENGINE
# define	COLLISIONENGINE

# include	"Engine.h"

class CollisionEngine : public Engine
{
public:
	CollisionEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL);
	~CollisionEngine();

	virtual void frame();
private:
};

#endif // !COLLISIONENGINE
