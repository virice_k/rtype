#ifndef		EVENTENGINE_H
# define	EVENTENGINE_H

#include	"Engine.h"
#include	"GameWindow.h"

#include	"AnimatedSprite.h"

class EventEngine : public Engine
{
public:
	EventEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL);
	~EventEngine();

	virtual void frame();
	int				getPowerMissile() const;

private:
	sf::Event			event;
	GameWindow			&window;
	int					powerMissile;

	Animation walkingAnimation;
	AnimatedSprite animatedSprite;
};

#endif // !EVENTENGINE_H
