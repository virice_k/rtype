#ifndef		NETWORKENGINE_H
# define	NETWORKENGINE_H

# include	"Engine.h"
# include	"UdpSocket.h"
# include	"PackageRFC.h"

class		NetworkEngine : public Engine
{
public:
	NetworkEngine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL);
	~NetworkEngine();

	virtual void frame();

	bool	sendMyPos(std::string const& address, unsigned int port);
	bool	sendConnectMsg(std::string const& address, unsigned int port);

	void	setIpServer(std::string const& s);

private:
	UdpSocket	socket;
	PackageRFC	p;
	std::string	ipServer;
};

void	*routineNetworkEngine(void *arg);

#endif // !NETWORKENGINE_H
