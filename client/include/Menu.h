#include	<cstdlib>
#include	<SFML\Graphics.hpp>
#include	<iostream>
#include	"Game.h"
#include	"GameWindow.h"

class Menu
{
public:
	Menu();
	~Menu();
	void		CheckPos(int dir);
	bool		DrawMenu();
	void		CheckMenu();
	void		CheckIp();
	void		FillIpServer(char input);
	void		BackSpace();
	std::string const &GetIpserver() const;

private:
	std::string			ipserver;
	bool				rungame;
	bool				runmenu;
	sf::Text			start;
	sf::Text			options;
	sf::Text			quitter;
	sf::Text			connexion;
	sf::Text			ipinput;
	sf::Text			ip;
	sf::Text			ipcurrent;
	sf::Font			font;
	sf::Event			event;
	int					pos;
	std::string			text;
	sf::RectangleShape  InputBar;
};