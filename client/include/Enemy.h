#ifndef		ENEMY_H
# define	ENEMY_H

# include	"Entity.h"
# include	"GameWindow.h"

class		Enemy : public Entity
{
public:
	Enemy(sf::Texture const& t, sf::IntRect const& r, float speed, float x = WIN_WIDTH, float y = WIN_HEIGHT / 2);
	~Enemy();

	virtual void	update();

private:

};

#endif // !ENEMY_H
