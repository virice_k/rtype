#ifndef		ENTITY_H
# define	ENTITY_H

# include	<SFML/Graphics/RenderWindow.hpp>
# include	<SFML/Graphics/Sprite.hpp>

# include	"AnimatedSprite.h"

class		Entity
{
public:
	virtual ~Entity();

	virtual void	update() = 0;

	virtual void	draw();
	
	void	setPosition(float x, float y);
	void	setX(float x);
	void	setY(float y);

	float	getX() const;
	float	getY() const;

	void	moveUp();
	void	moveDown();
	void	moveLeft();
	void	moveRight();

	void	addAnimStep(sf::IntRect const& rect);
	void	setAnimSpeed(sf::Time time);
	void	setAnimType(AnimatedSprite::Type t);

	sf::IntRect const& getTextureRect() const;
	sf::FloatRect	getLocalBounds() const;
	sf::FloatRect	getGlobalBounds() const;

protected:
	Entity(sf::Texture const& t, sf::IntRect const& r, float speed, float x = 0, float y = 0);

	float			speed;
	float			posX;
	float			posY;
	Animation		animation;
	AnimatedSprite	animSprite;
};

#endif // !ENTITY_H
