#ifndef		ENGINE_H
# define	ENGINE_H

# include	<list>
# include	<map>
# include	<SFML/Graphics.hpp>

# include	"Player.h"
# include	"Missile.h"
# include	"Enemy.h"
# include	"Explosion.h"

class		Engine
{
public:
	Engine(std::map<std::string, sf::Texture> &texturesM, std::map<std::string, Player*> &playersM, std::list<Missile*> &missilesL, std::list<Enemy *> &enemyL, std::list<Explosion *> &explosionL);
	virtual	~Engine();

	virtual void frame() = 0;
protected:
	std::map<std::string, sf::Texture>	&texturesM;

	std::map<std::string, Player*>		&playersM;

	std::list<Missile *>				&missilesL;
	std::list<Enemy *>				&enemyL;
	std::list<Explosion *>				&explosionL;
};

#endif // !ENGINE_H
