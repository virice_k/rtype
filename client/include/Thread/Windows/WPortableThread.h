#ifndef		WPORTABLETHREAD_H
# define	WPORTABLETHREAD_H

# ifdef			_WIN32

#  include			<windows.h>

typedef				void		*(*threadRoutinePointer)(void *);
typedef				LPVOID						threadArgPointer;
typedef				HANDLE						threadHandle;
typedef				DWORD						threadId;

# endif			// _WIN32

#endif		// !WPORTABLETHREAD_H
