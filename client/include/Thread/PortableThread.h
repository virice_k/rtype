#ifndef		PORTABLETHREAD_H
# define	PORTABLETHREAD_H

#ifdef	_WIN32
#include	"./Windows/WPortableThread.h"
#else	// UNIX
#include	"./Unix/UPortableThread.h"
#endif // _WIN32

class				PortableThread
{
public:
	PortableThread();
	PortableThread(threadRoutinePointer	startRoutine);
	PortableThread(threadRoutinePointer startRoutine, threadArgPointer arg);
	~PortableThread();

	void			setRoutine(threadRoutinePointer	startRoutine);
	void			setArg(threadArgPointer arg);

	void			start();
	void			wait();
	void			exit();

private:
	threadRoutinePointer		tStartRoutine;
	threadArgPointer			tArg;
	threadHandle				tHandle;
	threadId					tId;
};

#endif // !PORTABLETHREAD_H
