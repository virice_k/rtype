#ifndef		UPORTABLETHREAD_H
# define	UPORTABLETHREAD_H

# ifndef			_WIN32

#  include			<pthread.h>

typedef				void		*(*threadRoutinePointer)(void *);	
typedef				void *						threadArgPointer;
typedef				pthread_t					threadHandle;
typedef				pthread_t					threadId;

# endif			// _WIN32

#endif		// !WPORTABLETHREAD_H
