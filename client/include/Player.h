#ifndef		PLAYER_H
# define	PLAYER_H

# include	<string>
# include	<SFML/Graphics/RenderWindow.hpp>
# include	<SFML/Graphics/Sprite.hpp>

# include	"Entity.h"

class Player : public Entity
{
public:
	Player(sf::Texture const& texture, sf::IntRect const& rect, float x = 0, float y = 0, std::string const& name = "Player 1");
	~Player();

	virtual void	update();

	void		moveUp();
	void		moveDown();
	void		moveLeft();
	void		moveRight();

	int			getScore() const;
	void		setScore(int s);
private:
	std::string		name;
	int				score;
	int				lifes;
};

#endif //!PLAYER_H
