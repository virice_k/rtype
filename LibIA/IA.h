#ifndef IA_H_
# define IA_H_

# ifdef	_WIN32
#  define DLL_EXPORT		__declspec(dllexport)
# else	// UNIX
#  define DLL_EXPORT
# endif // _WIN32

# include "IIA.h"

class IA : public IIA
{
public: 
	IA();
	~IA();

	void	getNextCoord(float x, float y, float &newX, float &newY);
};

#endif // !IA_H_
