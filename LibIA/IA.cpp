#include "IA.h"

IA::IA()
{

}

IA::~IA()
{

}

void		IA::getNextCoord(float x, float y, float &newX, float &newY)
{
	newX = x - 1;
	newY = y;
}

extern "C"
{
	DLL_EXPORT
	IA	*entrypoint(void) 
	{
		return (new IA());
	}
}