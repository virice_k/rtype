#ifndef IIA_H_
# define IIA_H_

class IIA
{
public:
	virtual ~IIA() {}

	virtual void	getNextCoord(float x, float y, float &newX, float &newY) = 0;

};

#endif // !IIA_H_
