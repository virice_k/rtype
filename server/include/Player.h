#ifndef		PLAYER_H_
# define	PLAYER_H_

# include	<string>
# include	<Entity.h>

class Player : public Entity
{
public:
	Player(void);
	~Player(void);

	int getId() const;
	std::string getName() const;

	void setName(std::string _name);
	void setId(int _id);
	 void			setRemoteAdress(std::string remoteAdress);
			 void setRemotePort(unsigned short _remotePort);

	std::string	getRemoteAdress() const;
	unsigned short	getRemotePort() const;
	bool	getAlive() const;
	void	setAlive(bool _alive);
	void	setScore();
	std::string idAndScore();

private:
	bool alive;
	int id;
	std::string name;
	std::string remoteAdress;
	unsigned short remotePort;
	int score;
};

#endif
