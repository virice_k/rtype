#include <list>

class Enemy;
class PackageRFC;
class Player;
class CollisionChecker;
class Entity;

class GameData
{

public:
	GameData(void);
	~GameData(void);

	void	update();
	void	addPlayer(Player* _player);
	
	std::list<Entity*>  getMobList() const;
	std::list<Player*>	getPlayerList() const;



private:
	void checkCollision();

	std::list<Entity*>	mobList;
	std::list<Player*> playerList;
	CollisionChecker *collision;
	//std::list<>
};

