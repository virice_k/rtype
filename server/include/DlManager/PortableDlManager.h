#ifndef		PORTABLEDLMANAGER
# define	PORTABLEDLMANAGER

# include	<map>

# include	"PortableDlLoader.h"

class		PortableDlManager
{
public:
	PortableDlManager();
	~PortableDlManager();

	/*
	**	Load the library with the libraryPath and save it. libraryIdent will be used after to ident the library
	**	If libraryIdent is not specified the library will be saved with its name without extension
	**	The function return false if the library failed to load of if the ident already exist
	*/
	bool		loadLibrary(std::string const& libraryPath, std::string const& libraryIdent = "");

	/*
	** Load all library in the dir
	** Return the number of libraries loaded
	*/
	int			loadLibFromDir(std::string const& dirPath);

	/*
	** Return instance of object from library.
	** Library had to be load before
	*/
	template<typename T>
	T*		getInstanceFromLib(std::string const& libraryIdent, std::string const& entrypoint = "entrypoint");

private:
	std::map<std::string, libraryHandle>	libraryHandles;
	PortableDlLoader						libLoader;
};

#ifdef    _WIN32
#include	"../../src/DlManager/Windows/WPortableDlManager.cpp"
#else	// UNIX
#include	"../../src/DlManager/Unix/UPortableDlManager.cpp"
#endif // _WIN32

#endif // !PORTABLEDLMANAGER
