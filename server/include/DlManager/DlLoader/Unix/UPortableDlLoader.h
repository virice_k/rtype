#ifndef		UPORTABLEDLLOADER_H
# define	UPORTABLEDLLOADER_H

# ifndef	_WIN32

#  include <dlfcn.h>

typedef	void*	libraryHandle;

# endif		// _WIN32

#endif // !WPORTABLEDLLOADER_H