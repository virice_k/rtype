#ifndef		WPORTABLEDLLOADER_H
# define	WPORTABLEDLLOADER_H

# ifdef		_WIN32

#  include	<Windows.h>

typedef	HINSTANCE	libraryHandle;

# endif		// _WIN32

#endif // !WPORTABLEDLLOADER_H