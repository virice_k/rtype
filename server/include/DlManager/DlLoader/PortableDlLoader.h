#ifndef		PORTABLEDLLOADER_H
# define	PORTABLEDLLOADER_H

# include	<string>

#ifdef	_WIN32
#include	"./Windows/WPortableDlLoader.h"
#else	// UNIX
#include	"./Unix/UPortableDlLoader.h"
#endif // _WIN32

class		PortableDlLoader
{
public:
	PortableDlLoader();
	~PortableDlLoader();

	bool					load(std::string const& libraryName);
	void					unload();
	void					setHandle(libraryHandle handle);
	libraryHandle const&	getHandle() const;
	template<typename T>
	T*		getInstance(std::string const& entrypoint = "entrypoint");
private:
	libraryHandle	lHandle;
};

#ifdef	_WIN32
#include	"../../src/DlManager/DlLoader/Windows/WPortableDlLoader.cpp"
#else	// UNIX
#include	"../../src/DlManager/DlLoader/Unix/UPortableDlLoader.cpp"
#endif // _WIN32

#endif // !PORTABLEDLLOADER_H
