#ifndef		PORTABLESOCKET_H
# define	PORTABLESOCKET_H

# include	<string>

# ifdef	_WIN32
#  include	"./Windows/WPortableSocket.h"
# else	// UNIX
#  include	"./Unix/UPortableSocket.h"
# endif // _WIN32

class						PortableSocket
{
public:
	virtual ~PortableSocket();

protected:
	enum Type
    {
        tcp,
        udp
    };

	PortableSocket(Type t);

	virtual void			create();
	virtual void			create(SocketHandle handle);
	virtual void			close();

	Type					t;
	SocketHandle			sock;
private:
	PortableSocket(PortableSocket const& other);
	PortableSocket&		operator=(PortableSocket const& other);
};

#endif // !PORTABLESOCKET_H
