#ifndef		WPORTABLESOCKET_H
# define	WPORTABLESOCKET_H

# ifdef		_WIN32

#  include	<winsock2.h>

typedef		SOCKET	SocketHandle;

#endif	// _WIN32

#endif // !WPORTABLESOCKET_H
