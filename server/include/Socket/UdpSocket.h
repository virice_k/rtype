#ifndef		UDPSOCKET_H
# define	UDPSOCKET_H

# include	"PortableSocket.h"

class UdpSocket : public PortableSocket
{
 public:
  UdpSocket();
  ~UdpSocket();

  bool			bind(unsigned short port);
  int			send(void const *data, size_t size,
			     std::string const& remoteAddress,
			     unsigned short remotePort);
  int			receive(void *data, size_t size,
				std::string &remoteAddress,
				unsigned short &remotePort);

};
#endif // !UDPSOCKET_H
