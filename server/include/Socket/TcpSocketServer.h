#ifndef		TCPSOCKETSERVER_H
# define	TCPSOCKETSERVER_H

# include	"PortableSocket.h"
# include	"TcpSocketClient.h"

class TcpSocketServer : public PortableSocket
{
public:
	TcpSocketServer();
	~TcpSocketServer();

	bool			listen(unsigned short port); 
	bool			accept(TcpSocketClient& sockClient);
};

#endif // !TCPSOCKETSERVER_H
