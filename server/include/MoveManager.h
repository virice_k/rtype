#ifndef		MOVEMANAGER_H_
# define	MOVEMANAGER_H_

class MoveManager
{
public:

  MoveManager();
  MoveManager(MoveManager const &other);
  ~MoveManager();

  MoveManager &operator=(MoveManager const &other);

  /* Getters */


  /* Setters */


private:


};


#endif		/* !MOVEMANAGER_H_ */
