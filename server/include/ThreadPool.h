#ifndef		THREADPOOL_H_
# define	THREADPOOL_H_

class ThreadPool
{
public:

  ThreadPool();
  ThreadPool(ThreadPool const &other);
  ~ThreadPool();

  ThreadPool &operator=(ThreadPool const &other);

  /* Getters */


  /* Setters */


private:


};


#endif		/* !THREADPOOL_H_ */
