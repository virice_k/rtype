#ifndef		GAMESESSION_H_
# define	GAMESESSION_H_

# include	<iostream>
# include	<list>
# include	<mutex>

class Player;
class GameData;
class UdpSocket;
class PackageRFC;

//typedef void (GameSession::*session_start)(void*);

class GameSession
{
public:

  GameSession();
  GameSession(GameSession const &other);
  ~GameSession();

  GameSession &operator=(GameSession const &other);

  void run(void);
  /* Getters */

  int getPlayersNumber() const;
  int getMaxPlayer() const;
  /* Setters */
  void addPlayer(PackageRFC *data, std::string remoteAdress, unsigned short remotePort);
  void setGameData(GameData *_gameData);
  GameData* getGameData() const;
  void setSessionMutex(std::mutex *mutex);
  std::mutex* getMutex() const;
  void setSocket(UdpSocket *_socket);
  int	getId() const;
    	void setId(int _id);
		bool sessionIsOn() const;
		void	sessionQuit();

  void clientMoved(std::string remoteAdress, PackageRFC *package);
  void clientUpdatedName(std::string remoteAdress, PackageRFC *package);
  void clientQuit(std::string remoteAdress, PackageRFC *package);
  void clientFire(std::string remoteAdress, PackageRFC *package);

private:
  void	serverDied();
  void	serverEnemysPosition();
  void	serverFriendPosition();
  void	serverGameOver();


	GameData *gameData;
	  UdpSocket *socket;


	  bool gameOn;
	int playersMax;
	int id;

	std::mutex *mutex;


	void sendGameInfo();

};


#endif		/* !GAMESESSION_H_ */
