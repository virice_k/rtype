#ifndef		PORTABLEMUTEX_H
# define	PORTABLEMUTEX_H

#ifdef	_WIN32
#include	"./Windows/WPortableMutex.h"
#else	// UNIX
#include	"./Unix/UPortableMutex.h"
#endif // _WIN32

class PortableMutex
{
public:
	PortableMutex();
	~PortableMutex();

	void		init();
	void		destroy();
	void		lock();
	bool		trylock();
	void		unlock();

private:
	bool		isInit;
	mutexHandle	mHandle;
};

#endif		// !PORTABLEMUTEX_H
