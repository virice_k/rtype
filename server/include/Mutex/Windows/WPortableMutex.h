#ifndef			WPORTABLEMUTEX_H
# define		WPORTABLEMUTEX_H

# ifdef _WIN32

#  include		<windows.h>

typedef			CRITICAL_SECTION	mutexHandle;

# endif // _WIN32

#endif // !WPORTABLEMUTEX_H
