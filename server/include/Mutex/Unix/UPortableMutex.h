#ifndef			UPORTABLEMUTEX_H
# define		UPORTABLEMUTEX_H

# ifndef _WIN32

#  include		<pthread.h>

typedef			pthread_mutex_t		mutexHandle;

# endif // _WIN32

#endif // !UPORTABLEMUTEX_H