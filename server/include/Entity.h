#ifndef		ENTITY_H
# define	ENTITY_H

# include	<iostream>
# include	<sstream>

typedef enum e_entityType
{
	SCENERY,
	PLAYER,
	ENEMY
}			t_entType;

class Entity
{
public:
	virtual ~Entity();
	
	void		setPosition(int x, int y);
	void		setX(int x);
	void		setY(int y);
	void		setId(int id);
	void		setType(t_entType type);
	void		setHeight(int height);
	void		setWidth(int width);
	std::string	description() const;

	int			getX() const;
	int			getY() const;
	int			getId() const;
	t_entType	getType() const;
	int			getHeigth() const;
	int			getWidth() const;

protected:
	Entity(int x = 0, int y = 0, int height = 0, int width = 0, t_entType type = SCENERY);

private:
	int			posX;
	int			posY;
	int			id;
	int			height;
	int			width;
	t_entType	type;

	Entity(Entity const& other);
	Entity&		operator=(Entity const& other);
};

#endif // !ENTITY_H
