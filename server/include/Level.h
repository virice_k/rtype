#ifndef		LEVEL_H_
# define	LEVEL_H_

class Level
{
public:

  Level();
  Level(Level const &other);
  ~Level();

  Level &operator=(Level const &other);

  /* Getters */


  /* Setters */


private:


};


#endif		/* !LEVEL_H_ */
