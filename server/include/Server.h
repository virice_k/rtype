#ifndef		SERVER_H_
# define	SERVER_H_

# include	<list>
# include	<mutex>
# include	<map>
# include	<iostream>
# include	<exception>

class UdpSocket;
class GameSession;
class PortableThread;
class GameData;
class PackageRFC;

/*
class myexception: public std::exception
{
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
} myex;*/

class Server
{
public:

  Server();
  Server(Server const &other);
  ~Server();

  Server &operator=(Server const &other);

  bool	connectServer(int port);
  bool	run();
  bool	exitServer();

  UdpSocket *socket;
  /* Getters */


  /* Setters */


private:
void		connectionPlayer();
void		stopSession();
GameSession*		createGameSession();
bool		joinGameSession(std::string remoteAdress, unsigned short remotePort, PackageRFC *package);
bool		updateData(std::string remoteAdress, PackageRFC package);

std::list<PortableThread*>	gameThreadList;
std::list<GameData*>		gameDataList;
std::list<GameSession*>		gameSessionList;

std::list<std::mutex*>		gameMutexList;

std::map<int, PortableThread*>	gameSessionCorrespondanceTable;
// take session id and remoteaddress
std::map<std::string, std::mutex*>	gameMutexMap;
//take remoteadresse and ptr on mutex
std::map<std::string, GameData*>	gameDataMap;
std::map<std::string, GameSession*>		gameSessionMap;

//take remoteadress and ptr on gameData

int sessionIDs;


};

#endif		/* !SERVER_H_ */
