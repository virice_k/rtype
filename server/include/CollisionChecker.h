#ifndef COLLISIONCHECKER_H_

# define COLLISIONCHECKER_H_
# include <list>
# include <Entity.h>

class CollisionChecker
{
public:
	CollisionChecker(void);
	~CollisionChecker(void);

	bool	checkAllCollisions(std::list<Entity *>, Entity *) const;
};

#endif // !COLLISIONCHECKER_H_