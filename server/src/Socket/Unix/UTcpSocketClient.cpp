#ifndef _WIN32

# include	<iostream>

# include	"TcpSocketClient.h"

TcpSocketClient::TcpSocketClient()
	: PortableSocket(tcp)
{
	this->create();
}

TcpSocketClient::~TcpSocketClient()
{
}

bool		TcpSocketClient::connect(std::string const& host, unsigned short port)
{
	struct	sockaddr_in		sin;
	int		ret;

	if (this->sock == INVALID_SOCKET)
		return false;

	sin.sin_addr.s_addr = inet_addr(host.c_str());
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);

	if ((ret = ::connect(this->sock, (struct sockaddr *)(&sin), sizeof(sin))) == -1)
		return false;
	return true;
}

int			TcpSocketClient::send(void const *data, size_t size)
{
	int		sendBytes;

	if (this->sock == INVALID_SOCKET)
		return false;

	if ((sendBytes = ::send(this->sock, data, size, 0)) == -1)
		return -1;

	return sendBytes;
}

int				TcpSocketClient::receive(void *data, size_t size)
{
	int			recvBytes;
	
	if (this->sock == INVALID_SOCKET)
		return false;

	if ((recvBytes = ::recv(this->sock, data, size, 0)) == -1)
		return -1;

	return recvBytes;
}

#endif // !_WIN32
