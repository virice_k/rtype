#ifndef		_WIN32

# include	"PortableSocket.h"

PortableSocket::PortableSocket(Type t)
  : t(t), sock(INVALID_SOCKET)
{
}

PortableSocket::~PortableSocket()
{
	if (this->sock != INVALID_SOCKET)
		this->close();
}

void	PortableSocket::create()
{
	if (this->sock == INVALID_SOCKET)
    {
        SocketHandle handle = socket(AF_INET, this->t == tcp ? SOCK_STREAM : SOCK_DGRAM, 0);
        create(handle);
    }
}

void		PortableSocket::create(SocketHandle handle)
{
	if (this->sock != INVALID_SOCKET)
		this->close();
	this->sock = handle;
}

void		PortableSocket::close()
{
  closesocket(this->sock);
  this->sock = INVALID_SOCKET;
}


#endif // !_WIN32
