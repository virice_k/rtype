#include "GameData.h"
#include "PackageRFC.h"
#include "Player.h"
#include "CollisionChecker.h"

GameData::GameData(void)
{
	this->collision = new CollisionChecker;
}


GameData::~GameData(void)
{
}

std::list<Entity*>  GameData::getMobList() const
{
	return this->mobList;
}

std::list<Player*>	GameData::getPlayerList() const
{
	return this->playerList;
}

void	GameData::checkCollision()
{
	std::list<Player*>::iterator it = this->playerList.begin();
	Player *player;
	int order = 1;
	bool gameOver = true;

	while (it != this->playerList.end())
	{
		player = (*it);
		player->setAlive(this->collision->checkAllCollisions(this->mobList, player));
		++it;
	}
}

void	GameData::update()
{
	this->checkCollision();
	// make enemy advance plus shoot
}

void	GameData::addPlayer(Player* player)
{
	this->playerList.push_back(player);
}