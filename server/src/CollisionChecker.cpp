#include "CollisionChecker.h"
#include <iostream>

CollisionChecker::CollisionChecker(void)
{
}


CollisionChecker::~CollisionChecker(void)
{
}


bool						CollisionChecker::checkAllCollisions(std::list<Entity *> entitiesList, Entity *entity) const
{
	std::list<Entity *>::iterator		it;

	it = entitiesList.begin();
	while (it != entitiesList.end())
	{
		if ((*it)->getX() >= entity->getX() && (*it)->getX() <= entity->getX() + entity->getWidth() &&
			(*it)->getY() >= entity->getY() && (*it)->getY() <= entity->getY() + entity->getHeigth()
			&& (*it)->getId() != entity->getId())
			return (true);
		++it;
	}
	return (false);
}