#include	"GameSession.h"
#include	"GameData.h"
#include	"Player.h"
#include	"UdpSocket.h"
#include	"PackageRFC.h"

GameSession::GameSession()
{
	this->playersMax = 4;
	this->gameOn = true;
}

GameSession::~GameSession()
{
}

void	GameSession::addPlayer(PackageRFC *data, std::string remoteAdress, unsigned short remotePort)
{

	Player* player = new Player;
		PackageRFC packageOut;
			int nb;

	player->setRemoteAdress(remoteAdress);
	player->setRemotePort(remotePort);
	nb = (this->gameData->getPlayerList()).size();
	nb = nb <= 0 ? 1 : nb;
	player->setId(nb);
	nb /= 520;

	player->setY(nb);
	player->setX(0);

	this->gameData->addPlayer(player);
	std::string info;
	std::ostringstream convert;
	convert << id;
	info = convert.str();

//	packageOut = new PackageRFC(1, 0x03, info.size(), 0x09, info); // order, id, size, type, value

	if (this->socket)
	{
		packageOut.setType(0x09);
		packageOut.replaceValue(info);
		packageOut.setSize(info.size());
		std::cout << this->socket->send(&packageOut.pack(), sizeof(t_package), remoteAdress, remotePort) << std::endl;
	}

}

bool	GameSession::sessionIsOn() const
{
	return this->gameOn;
}

void	GameSession::sessionQuit()
{
	this->gameOn = false;
}

int		GameSession::getMaxPlayer() const
{
	return this->playersMax;
}

int		GameSession::getPlayersNumber() const
{
	int nb = this->playersMax;

	this->mutex->lock();
	nb = (this->gameData->getPlayerList()).size();
	this->mutex->unlock();
	return nb;
}

void	GameSession::setSocket(UdpSocket *_socket)
{
	this->socket = _socket;
}

void	GameSession::setGameData(GameData *_gameData)
{
	this->gameData = _gameData;
}

GameData*	GameSession::getGameData() const
{
	return this->gameData;
}

void	GameSession::setSessionMutex(std::mutex *_mutex)
{
	this->mutex = _mutex;
}

std::mutex*	GameSession::getMutex() const
{
	return this->mutex;
}

void	GameSession::setId(int _id)
{
	this->id = _id;
}

int	GameSession::getId() const
{
	return this->id;
}

// client
void GameSession::clientMoved(std::string remoteAdress, PackageRFC *package)
{
	mutex->lock();

	mutex->unlock();
}

void GameSession::clientUpdatedName(std::string remoteAdress, PackageRFC *package)
{
	mutex->lock();

	mutex->unlock();
}

void GameSession::clientQuit(std::string remoteAdress, PackageRFC *package)
{
	mutex->lock();

	mutex->unlock();
}

void GameSession::clientFire(std::string remoteAdress, PackageRFC *package)
{
	mutex->lock();

	mutex->unlock();
}

// server
void	GameSession::serverDied()
{
	std::list<Player*> playerList = this->gameData->getPlayerList();

	std::list<Player*>::iterator it = playerList.begin();
	std::list<Player*>::iterator it2 = playerList.begin();

	Player *player = NULL;
	Player *player2 = NULL;
	PackageRFC *packageOut = NULL;
	int order = 1;
	bool gameOver = true;

	//std::cout << "join game session : ";

	while (it != playerList.end())
	{
		player = (*it);
		if (!player->getAlive()
			&& this->socket)
		{
				while (it2 != playerList.end())
				{
					player2 = (*it2);
					/*
					std::string info = enemy->description();
					std::ostringstream convert;

					info.append(", ");
					convert << mobList.size();
					info.append(convert.str());

					std::cout << "position enemy: " << info << std::endl; 
					packageOut = new PackageRFC(order, 0x00, info.length(), 0x04, info);  // order, id, size, type, value
					std::cout << this->socket->send((void*)packageOut, info.size(), player->getRemoteAdress(), player->getRemotePort()) << std::endl;
					order++;
					*/
			// send player death to all players
					++it2;
				}
		}
		++it;
	}
}

void	GameSession::serverEnemysPosition()
{
	std::list<Player*> playerList = this->gameData->getPlayerList();
	std::list<Entity*> mobList = this->gameData->getMobList();

	std::list<Player*>::iterator it = playerList.begin();
	std::list<Entity*>::iterator it2 = mobList.begin();

	Player *player = NULL;
	Entity *enemy = NULL;

	PackageRFC *packageOut = NULL;
	int order = 1;
	bool gameOver = true;

	while (it != playerList.end())
	{
		player = (*it);
		if (this->socket)
		{
				while (it2 != mobList.end())
				{
					enemy = (*it2);
					// send all enemies position to all players
						std::string info = enemy->description();
					std::ostringstream convert;

					info.append(", ");
					convert << mobList.size();
					info.append(convert.str());

					std::cout << "position enemy: " << info << std::endl; 
					packageOut = new PackageRFC(order, 0x00, info.length(), 0x04, info);  // order, id, size, type, value
					std::cout << this->socket->send((void*)packageOut, info.size() + 20, player->getRemoteAdress(), player->getRemotePort()) << std::endl;
					order++;
					++it2;
				}
		}
		++it;
	}
}

void	GameSession::serverFriendPosition()
{
		std::list<Player*> playerList = this->gameData->getPlayerList();

std::list<Player*>::iterator it = playerList.begin();
	std::list<Player*>::iterator it2 = playerList.begin();

	Player *player = NULL;
	Player *player2 = NULL;
	PackageRFC packageOut;

	//
	//
	int order = 1;
	bool gameOver = true;

	while (it != playerList.end())
	{
		player = (*it);
		if (this->socket)
		{
				while (it2 != playerList.end())
				{
					player = (*it2);
					std::string info = player->description();
					std::ostringstream convert;

					//"(position of player) * number of players)"
					info.append(", ");
					convert << playerList.size();
					info.append(convert.str());

					std::cout << "position friend: " << info << std::endl; 
					//packageOut = new PackageRFC(order, 0x03, info.length(), 0x0F, info);  // order, id, size, type, value
					//std::cout << this->socket->send((void*)packageOut, info.size() + 20, player->getRemoteAdress(), player->getRemotePort()) << std::endl;
					packageOut.setType(0x03);
					packageOut.replaceValue(info);
					packageOut.setSize(info.size());
	
					this->socket->send(&packageOut.pack(), sizeof(t_package), player->getRemoteAdress(), player->getRemotePort());
					order++;
			// send players position to to all players
					++it2;
				}
		}
		++it;
	}
}

void	GameSession::serverGameOver()
{
	std::list<Player*> playerList = this->gameData->getPlayerList();
	std::list<Player*>::iterator it = playerList.begin();
	Player *player = NULL;
	PackageRFC *packageOut = NULL;
	int order = 1;
	bool gameOver = true;

	while (it != playerList.end())
	{
		player = (*it);
		if (player->getAlive() == true)
		{
			gameOver = false;
			break;
		}
		++it;
	}
	if (this->socket && gameOver == true && playerList.size() > 0)
		{
			it = playerList.begin();
			while (it != playerList.end())
			{
						player = (*it);
			std::cout << "Game Over" << std::endl;
			
		std::string info = player->idAndScore();
		std::ostringstream convert;

					//"(position of player) * number of players)"
		info.append(" * ");
		convert << playerList.size();
		info.append(convert.str());

		packageOut = new PackageRFC(order, 0x03, info.length(), 0x07, info);

		std::cout << this->socket->send((void*)packageOut, info.size(), player->getRemoteAdress(), player->getRemotePort()) << std::endl;
		++order;

		++it;
			}
		}
}

void	GameSession::sendGameInfo()
{
//	this->serverEnemysPosition();
	this->serverFriendPosition();
	//this->serverDied();
	//this->serverGameOver();
}

void	GameSession::run()
{
	while (this->gameOn)
		{
		 if (this->mutex->try_lock())
		 {
			// this->gameData->update();
			this->sendGameInfo();
			//std::cout << "game session run " << std::endl;
			this->mutex->unlock();
		 }
	}	
}