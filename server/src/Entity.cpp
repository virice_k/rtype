#include	"Entity.h"
#include	<sstream>
#include	<string>
#include	<iomanip>

Entity::~Entity()
{
}

Entity::Entity(int x, int y, int height, int width, t_entType type)
	: posX(x), posY(y), height(height), width(width), type(type)
{
}

void	Entity::setPosition(int x, int y)
{
	this->posX = x;
	this->posY = y;
}

void	Entity::setX(int x)
{
	this->posX = x;
}

void	Entity::setY(int y)
{
	this->posY = y;
}

void	Entity::setType(t_entType type)
{
	this->type = type;
};

void	Entity::setHeight(int height)
{
	this->height = height;
}

void	Entity::setWidth(int width)
{
	this->width = width;
}

int	Entity::getX() const
{
	return this->posX;
}

int	Entity::getY() const
{
	return this->posY;
}

t_entType Entity::getType() const
{
	return this->type;
}

int		Entity::getHeigth() const
{
	return this->height;
}

int		Entity::getWidth() const
{
	return this->width;
}

int		Entity::getId() const
{
	return this->id;
}

std::string Entity::description() const
{
	std::string desc;
//"(position of player) * number of players)"
	std::ostringstream convert;

	convert << this->posX;
	desc.append(convert.str());
	desc.append(", ");

		std::ostringstream convert2;
	convert2 << this->posY;
	desc.append(convert2.str());
	return desc;
}