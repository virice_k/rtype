#include "PackageRFC.h"
#include <cstring>
#include <iostream>

PackageRFC::PackageRFC(int id, char type, char size, char fctId, std::string value)
	: _id(id), _type(type), _size(size), _function(fctId), _value(value)
{
	this->_time = 0;
}

PackageRFC::PackageRFC(t_package pack)
	: _id(pack.id), _type(pack.type), _size(pack.size), _function(pack.function), _value(pack.value), _time(pack.time)
{

}

PackageRFC::~PackageRFC()
{

}

int			PackageRFC::getId() const
{
	return (this->_id);
}

std::time_t		PackageRFC::getTime() const
{
	return (this->_time);
}

char		PackageRFC::getType() const
{
	return (this->_type);
}

char		PackageRFC::getSize() const
{
	return (this->_size);
}

char		PackageRFC::getFctId() const
{
	return (this->_function);
}

std::string	PackageRFC::getValue() const
{
	return (this->_value);
}

void		PackageRFC::setTime()
{
	this->_time = std::time(NULL);
}

void		PackageRFC::setType(char type)
{
	this->_type = type;
}

void		PackageRFC::setSize(char size)
{
	this->_size = size;
}

void		PackageRFC::addValue(std::string value)
{
	this->_value += value;
}

void		PackageRFC::delValue()
{
	this->_value = "";
}

void		PackageRFC::replaceValue(std::string value)
{
	this->_value = value;
}

t_package	PackageRFC::pack() const
{
	t_package	ret;

	ret.id = this->_id;
	ret.size = this->_size;
	ret.time = std::time(NULL);
	ret.function = this->_function;
	ret.type = this->_type;
	std::memset(ret.value, 0, ret.size + 1);
	std::memcpy(ret.value, (this->_value).c_str(), ret.size);
	return (ret);
}

void		PackageRFC::unPack(t_package const& pack)
{
	this->_id = pack.id;
	this->_type = pack.type;
	this->_size = pack.size;
	this->_time = pack.time;
	this->_function = pack.function;
	this->_value = pack.value;
}

void		PackageRFC::description() const
{
	std::cout << "package id: " << this->getId() << std::endl
		<< "type: " << (int)this->getType() << std::endl
		<< "size: " << (int)this->getSize() << std::endl
		<< "time: " << (int)this->getTime() << std::endl
		<< "value: " << this->getValue() << std::endl;
}