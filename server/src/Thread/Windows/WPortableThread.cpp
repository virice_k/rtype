#ifdef			_WIN32

# include		<iostream>

# include		"PortableThread.h"

PortableThread::PortableThread()
	: tStartRoutine(NULL), tArg(NULL)
{
}

PortableThread::PortableThread(threadRoutinePointer	startRoutine)
	: tStartRoutine(startRoutine), tArg(NULL)
{
}

PortableThread::PortableThread(threadRoutinePointer startRoutine, threadArgPointer arg)
	: tStartRoutine(startRoutine), tArg(arg)
{
}

PortableThread::~PortableThread()
{
	if (this->tHandle)
	CloseHandle(this->tHandle);
}

void			PortableThread::setRoutine(threadRoutinePointer	startRoutine)
{
	this->tStartRoutine = startRoutine;
}

void			PortableThread::setArg(threadArgPointer arg)
{
	this->tArg = arg;
}

void			PortableThread::start()
{
	if (this->tStartRoutine != NULL)
	{
		this->tHandle =  CreateThread(NULL, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(this->tStartRoutine), this->tArg, 0, &this->tId);
		if (!this->tHandle)
			std::cerr << "Cannot create thread" << std::endl;
	}
	else
		std::cerr << "Missing routine to call this thread" << std::endl;
}

void			PortableThread::wait()
{
	if (this->tId != GetCurrentThreadId())
		WaitForSingleObject(this->tHandle, INFINITE);
	else
		std::cerr << "WARNING : Thread " << this->tId << " try to wait for itself !" << std::endl;
}

void			PortableThread::exit()
{
	if (this->tHandle)
		TerminateThread(this->tHandle, 0);
}

#endif			// _WIN32