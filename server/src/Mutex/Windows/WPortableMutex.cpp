#ifdef			_WIN32

# include	"PortableMutex.h"

PortableMutex::PortableMutex()
	: isInit(false)
{
}

PortableMutex::~PortableMutex()
{
	if (this->isInit)
	{
		DeleteCriticalSection(&this->mHandle);
		this->isInit = false;
	}
}

void		PortableMutex::init()
{
	if (!this->isInit)
	{
		InitializeCriticalSection(&this->mHandle);
		this->isInit = true;
	}
}

void		PortableMutex::destroy()
{
	if (this->isInit)
	{
		DeleteCriticalSection(&this->mHandle);
		this->isInit = false;
	}
}

void		PortableMutex::lock()
{
	EnterCriticalSection(&this->mHandle);
}

bool		PortableMutex::trylock()
{
	if (TryEnterCriticalSection(&this->mHandle) != 0)
		return true;
	return false;
}

void		PortableMutex::unlock()
{
	LeaveCriticalSection(&this->mHandle);
}

#endif // _WIN32