#include "Player.h"

Player::Player(void)
{
	this->name = "player x";
	this->id = 0;
	this->alive = true;
}

Player::~Player(void)
{
}

void	Player::setName(std::string _name)
{
	this->name = _name;
}

void	Player::setId(int _id)
{
	this->id = _id;
}

std::string	Player::getName() const
{
	return this->name;
}

int	Player::getId() const
{
	return this->id;
}

bool	Player::getAlive() const
{
	return this->alive;
}

void	Player::setAlive(bool _alive)
{
	this->alive = _alive;
}


void	Player::setRemoteAdress(std::string _remoteAdress)
{
	this->remoteAdress = _remoteAdress;
}

void	Player::setRemotePort(unsigned short _remotePort)
{
	this->remotePort = _remotePort;
}

std::string	Player::getRemoteAdress() const
{
	return this->remoteAdress;
}

unsigned short	Player::getRemotePort() const
{
	return this->remotePort;
}

std::string		Player::idAndScore()
{
	std::string desc = "( ";
	std::ostringstream stream;

	stream << this->id;
	desc.append(stream.str());
	desc.append(", ");

	stream.flush();
	stream << this->score;
	desc.append(stream.str());
	desc.append(" )");
	return desc;
}