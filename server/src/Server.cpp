#include	<thread>

#include	"iostream"
#include	"Server.h"
#include	"UdpSocket.h"
#include	"GameSession.h"
#include	"GameData.h"
#include	"PortableThread.h"
#include	"PackageRFC.h"

Server::Server()
{
	this->socket = new UdpSocket();

	std::cout << "server as been created" << std::endl;
}

Server::~Server()
{
	delete this->socket;
	std::cout << "server got destroyed" << std::endl;
}

bool	Server::connectServer(int port)
{
	bool success = false;

	std::cout << "server connect on port " <<  port << std::endl;
	try
	{
		success = this->socket->bind(port);

	}
	catch (std::exception& e)
	{
		(void)e;
		std::cout << "binding socket fail" << std::endl;
		//std::cout << e.what() << std::endl;
	}
	return success;
}

bool	Server::exitServer()
{
	std::cout << "server exit" << std::endl;

	GameSession *session;
		std::list<GameSession*>::iterator it = this->gameSessionList.begin();
		std::string remoteAdress;

	while (it != this->gameSessionList.end())
	{
		session = (*it);
		session->sessionQuit();

		++it;
	}
	this->stopSession();
	return true;
}

void	Server::connectionPlayer()
{
	std::cout << "ici gestion connection" << std::endl;

	//close(log_fd);
}

inline void *startSessionRoutine(void* p)
{
	reinterpret_cast<GameSession*>(p)->run();
	return (NULL);
}

GameSession*	Server::createGameSession()
{
	GameSession	*newSession = new GameSession;
	GameData	*newData = new GameData;
	PortableThread *newThread = new PortableThread;
	std::mutex *newMutex = new std::mutex;
	static int id = 0;

	newSession->setId(id);
	newSession->setGameData(newData);
	newSession->setSessionMutex(newMutex);
	newSession->setSocket(this->socket);
	
	newThread->setArg(newSession);
	newThread->setRoutine(&startSessionRoutine);

	this->gameSessionCorrespondanceTable[id] = newThread;
	this->gameSessionList.push_back(newSession);
	//this->gameThreadList.push_back(newThread);
	//this->gameDataList.push_back(newData);
	++id;
	newThread->start();
	return newSession;
}

void	Server::stopSession()
{
	GameSession *session;
		std::list<GameSession*>::iterator it = this->gameSessionList.begin();

	while (it != this->gameSessionList.end())
	{
		session = (*it);
		if (session && session->sessionIsOn())
		{

			PortableThread *newThread = (this->gameSessionCorrespondanceTable.find(session->getId()))->second;
			newThread->exit();

			this->gameSessionList.erase(it++);
		}
		else
			++it;
	}
}

bool	Server::joinGameSession(std::string remoteAdress, unsigned short remotePort, PackageRFC *package)
{
	GameSession *session;
	GameData *data;
	std::list<GameSession*>::iterator it = this->gameSessionList.begin();
	std::mutex* mutex;
	int id;

	while (it != this->gameSessionList.end())
	{
		session = (*it);
		if (session->getPlayersNumber() < session->getMaxPlayer() && session->sessionIsOn())
		{
			mutex = session->getMutex();

			mutex->lock();
			
				data = session->getGameData();
				id = session->getId();

			//	this->gameSessionCorrespondanceTable[id] = remoteAdress;
				this->gameDataMap[remoteAdress] = data;
				this->gameMutexMap[remoteAdress] = mutex;
				this->gameSessionMap[remoteAdress] = session;

				session->addPlayer(package, remoteAdress, remotePort);


				mutex->unlock();
			

			return true;
		}

		++it;
	}
	return false;
}

bool	Server::updateData(std::string remoteAdress, PackageRFC package)
{
	std::mutex *mutex = (this->gameMutexMap.find(remoteAdress))->second;
	GameData *data = (this->gameDataMap.find(remoteAdress))->second;

	std::cout << "server try lock" << std::endl;
	for (int i = 0; i < 100; ++i)
	{
		if (mutex->try_lock())
		{
			std::cout << "locked mutex" << std::endl;
			data->update();
			mutex->unlock();
			std::cout << "unlocked mutex" << std::endl;
			return true;
		}
	}
	return false;
}

bool	Server::run()
{
	//std::thread connections(&Server::connectionPlayer, this);

	char data[281];
	size_t size;
	std::string remoteAddress;
	PackageRFC *packageIn;
	GameSession* session;
					std::mutex* mutex;
					int toto = 0;
		unsigned short remotePort;

	//PackageRFC *packageOut;
	t_package* p;

	size = 280;
	//this->joinGameSession();
	while (42)
	{
		// size == taille buffer;
		// remoteAdresse == adress du mecs qui ma ecrit
		memset(data, 0, 281);
		size = this->socket->receive(data, size, remoteAddress, remotePort);		
		
		//mutex = (this->gameMutexMap.find(remoteAddress))->second;

		if (size > 0)
		{
			std::cout << "size: " << size << "data: " << data << std::endl;
			p = (t_package*)data;
			packageIn = new PackageRFC(*p);
			packageIn->description();

			std::cout << (int)packageIn->getType() << std::endl;
			switch (packageIn->getType())
			{
			case 0x03: //id 0x03 type 0x03
				std::cout << "new connection" << std::endl;

				if (!this->joinGameSession(remoteAddress, remotePort, NULL))
				{

					session = this->createGameSession();
					if (session)
					{
						mutex = session->getMutex();
						mutex->lock();
						session->addPlayer(NULL, remoteAddress, remotePort);
						mutex->unlock();
					}

				}

				break;
			case 0x0A:
						session = (this->gameSessionMap.find(remoteAddress))->second;

				if (session)
				{
					session->clientMoved(remoteAddress, NULL);
					std::cout << "case 1, client send move commande" << std::endl;
				}
				
				break;
			case 0x0B:
						session = (this->gameSessionMap.find(remoteAddress))->second;

				if (session)
				{
					session->clientUpdatedName(remoteAddress, NULL);
					std::cout << "case 2, client send name-commande" << std::endl;

				}
				break;
			case 0x05: // type 0x01
						session = (this->gameSessionMap.find(remoteAddress))->second;

				if (session)
				{

					session->clientQuit(remoteAddress, NULL);
					this->stopSession();

					std::cout << "case 3: client send end of connection" << std::endl;
				}
				break;
			case 0x06: // type 0x03
						session = (this->gameSessionMap.find(remoteAddress))->second;

				if (session)
				{
					session->clientFire(remoteAddress, NULL);
					std::cout << "case 3: client send fire" << std::endl;

				}

				break;


			default:
				break;

			}
		
			//break;
			//	std::cout << this->socket->send(data, 7, remoteAddress, this->remotePort) << std::endl;
		}
		else
			break;
		// if data type is type connection (0x03)
		//		if (this->joinGameSession() == false)
		//			this->createGameSession();
		// else find player with remotePort x
	}

	std::cout << this->gameSessionList.size() << std::endl;
	std::cout << "server run" << std::endl;

	//connections.join();
	return true;
}
