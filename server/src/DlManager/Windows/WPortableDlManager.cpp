#ifdef _WIN32

# include	<iostream>
# include	<string>
# include	<windows.h>
# include	<tchar.h> 
# include	<stdio.h>
# include	<strsafe.h>

# include	"PortableDlManager.h"

PortableDlManager::PortableDlManager()
{
}

PortableDlManager::~PortableDlManager()
{
}

bool			PortableDlManager::loadLibrary(std::string const& libraryPath, std::string const& libraryIdent)
{
	bool		res;
	std::string	ident;

	if (libraryIdent != "")
		ident = libraryIdent;
	else
		ident = libraryPath;
	if (this->libraryHandles.find(ident) != this->libraryHandles.end())
		return false;

	res = this->libLoader.load(libraryPath);
	if (res)
	{
		this->libraryHandles[ident] = this->libLoader.getHandle();
		std::cout << "Library loaded on name : " << ident << std::endl;
		return true;
	}
	return false;
}

int					PortableDlManager::loadLibFromDir(std::string const& dirPath)
{
	WIN32_FIND_DATA	ffd;
	TCHAR			szDir[MAX_PATH];
	size_t			length_of_arg;
	HANDLE			hFind = INVALID_HANDLE_VALUE;
	DWORD			dwError = 0;
	std::string		dllPath;
	size_t			extension;
	int				nbLibLoad = 0;

	StringCchLength(dirPath.c_str(), MAX_PATH, &length_of_arg);
	if (length_of_arg > (MAX_PATH - 3))
		return nbLibLoad;
	
	StringCchCopy(szDir, MAX_PATH, dirPath.c_str());
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
		return nbLibLoad;
	do
   {
	   dllPath = ffd.cFileName;
	   extension = dllPath.find_last_of(".");
	   if (extension != std::string::npos)
	   {
		if ((dllPath.substr(dllPath.find_last_of(".")) == ".dll"))
			 if (this->loadLibrary(dirPath + "/" + dllPath, dllPath))
				 ++nbLibLoad;
	   }
   }
   while (FindNextFile(hFind, &ffd) != 0);

   return nbLibLoad;
}

template<typename T>
T*		PortableDlManager::getInstanceFromLib(std::string const& libraryIdent, std::string const& entrypoint)
{
	libraryHandle	tmpHandle;
	T				*res;

	tmpHandle = this->libLoader.getHandle();

	if (this->libraryHandles.find(libraryIdent) == this->libraryHandles.end())
		return NULL;

	this->libLoader.setHandle(this->libraryHandles[libraryIdent]);

	res = this->libLoader.getInstance<T>(entrypoint);

	this->libLoader.setHandle(tmpHandle);
	return res;
}

#endif // !_WIN32
