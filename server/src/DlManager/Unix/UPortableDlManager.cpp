#ifndef _WIN32

# include	<iostream>
# include	<string>

# include	"PortableDlManager.h"

PortableDlManager::PortableDlManager()
{
}

PortableDlManager::~PortableDlManager()
{
}

bool			PortableDlManager::loadLibrary(std::string const& libraryPath, std::string const& libraryIdent)
{
	bool		res;
	std::string	ident;

	if (libraryIdent != "")
		ident = libraryIdent;
	else
		ident = libraryPath;
	if (this->libraryHandles.find(ident) != this->libraryHandles.end())
		return false;

	res = this->libLoader.load(libraryPath);
	if (res)
	{
		this->libraryHandles[ident] = this->libLoader.getHandle();
		std::cout << "Library loaded on name : " << ident << std::endl;
		return true;
	}
	return false;
}

int					PortableDlManager::loadLibFromDir(std::string const& dirPath)
{
  (void)dirPath;
   return 0;
}

template<typename T>
T*		PortableDlManager::getInstanceFromLib(std::string const& libraryIdent, std::string const& entrypoint)
{
	libraryHandle	tmpHandle;
	T				*res;

	tmpHandle = this->libLoader.getHandle();

	if (this->libraryHandles.find(libraryIdent) == this->libraryHandles.end())
		return NULL;

	this->libLoader.setHandle(this->libraryHandles[libraryIdent]);

	res = this->libLoader.getInstance<T>(entrypoint);

	this->libLoader.setHandle(tmpHandle);
	return res;
}

#endif // !_WIN32
